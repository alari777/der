require('dotenv').config();
const wrappedFetch = require('socks5-node-fetch');
const fetch2 = require('node-fetch');
const Mysql = require('../mysql');
const Restapi = require('../restapi');

class Api {

    constructor(...params) {

        this.clientId = params[0];
        this.clientSecret = params[1];
        this.redis = params[2];
        this.counterForExpiresIn = params[3];
        this.counterForExpiresInRe = params[3];
        this.user = params[4];
        this.domain = params[5];

        this.accessToken = '';
        this.expiresIn = '';

        this.headers = {};

        this.time = 10;

        this.lastPrice = 0;
        this.indexPrice = 0;
        this.oldLastPrice = 0;
        this.lastPriceHedge = 0;
        this.activePosition = {};
        this.activePositionOldSize = 0;
        this.directionOld = '';
        this.hedgeActivePosition = {};

        this.hedgeBuyObj = {};
        this.hedgeSellObj = {};

        this.gridBuy = [];
        this.gridSell = [];

        this.amount = 0;
        this.amountBuyDirection = 0;
        this.amountSellDirection = 0;

        this.amountBuyStopDirection = 0;
        this.amountSellStopDirection = 0;

        this.gridStep = params[6]; // process.env.D_GRID_STEP; // 10;
        this.gridOrders = params[7]; // process.env.D_GRID_ORDERS; // 15;
        this.gridOrderSize = params[8]; // process.env.D_GRID_ORDER_SIZE; // 50;
        this.gridOrderSizePer = params[22]; // process.env.D_GRID_ORDER_SIZE; // 50;
        this.gridFirstOrder = params[23]; // process.env.D_GRID_ORDER_SIZE; // 50;

        this.hedgeOffset = params[9]; // process.env.D_HEDGE_OFFSET; // 50;
        this.userId = params[10];

        this.stepMultiplier = params[11];
        this.sizeMultiplier = params[12];
        this.hedgeOrStoploss = params[13];

        this.redisSub = params[14];
        this.redisPub = params[15];

        this.stoplossStep = params[16];

        this.hedgeBuyCounter = 0;
        this.hedgeSellCounter = 0;

        this.checkSizes = [];
        this.checkSizesBuy = [];
        this.checkSizesSell = [];

        this.checkPrices = [];

        this.middlePriceBuy = 0;
        this.countSizeBuy = 0;
        this.middlePriceSell = 0;
        this.countSizeSell = 0;

        this.lastBuyOrderPrice = 0;
        this.lastSellOrderPrice = 0;

        this.firstZeroMoving = 0;
        this.flagGetActions = 0;

        this.flagActRobotPause = params[17]; // 0

        this.openOrdersLimitBuyIds = [];
        this.openOrdersLimitSellIds = [];

        this.openOrdersStoplossBuyIds = [];
        this.openOrdersStoplossSellIds = [];

        this.openOrdersBuys = [];
        this.openOrdersSells = [];

        this.lastBuyOrderId = '';
        this.lastSellOrderId = '';

        this.globalStopFlag = 0;

        this.flagPauseWhenDerebitDisRedis = 0;

        this.customTake = params[18];
        this.longShort = params[19];

        this.stepMultiplierSign = params[20];
        this.sizeMultiplierSign = params[21];

        this.getGridSettings = 0;
        this.getCustomGridsSettings = 0;
        this.sideLevelLow = params[24];
        this.sideLevelHigh = params[25];

        this.gridsTypes = params[26];

        if (this.gridsTypes === 0) {
            this.customGridsBuy = [];
            this.customGridsBuy = [];
        }

        if (this.gridsTypes === 1) {
            try {
                if (params[27] !== '' && params[27] !== '0' && params[27] !== 0) {
                    this.customGridsBuy = JSON.parse(params[27]).map(value => {
                        return value;
                    });
                } else {
                    this.customGridsBuy = [];
                }
            } catch (err) {
                this.customGridsBuy = [];
                console.log(err.message)
            }
        }

        if (this.gridsTypes === 1) {
            try {
                if (params[28] !== '' && params[28] !== '0' && params[28] !== 0) {
                    this.customGridsSell = JSON.parse(params[28]).map(value => {
                        return value;
                    });
                } else {
                    this.customGridsSell = [];
                }
            } catch (err) {
                this.customGridsSell = [];
                console.log(err.message)
            }
        }

        this.customGridsBuySlPrice = params[29];
        this.customGridsBuySlSize = params[30];
        this.customGridsSellSlPrice = params[31];
        this.customGridsSellSlSize = params[32];

        this.errorSellBuy = 0;

        if (process.env.D_USER_ID === '37') {
            this.fetch = fetch2;
        } else {
            if (process.env.D_USER_ID === '6' || process.env.D_USER_ID === '8') {
                this.fetch = wrappedFetch({
                    socksHost: '185.30.233.137',
                    socksPort: '30100'
                });
            } else {
                this.fetch = wrappedFetch({
                    socksHost: params[33],
                    socksPort: params[34]
                });
            }
        }
        if (process.env.D_USER_ID === '6') {
            console.log( params[33],  ' ;;;;; ', params[34])
        }
        return this;
    }

    async auth() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'public/auth';
            const params = `?client_id=${this.clientId}&client_secret=${this.clientSecret}&grant_type=client_credentials&scope=${this.user}`;
            const res = await this.fetch(`${url}${method}${params}`);
            const body = await res.json();

            if (typeof body.error !== "undefined") {
                this.accessToken = '';
                console.error('accessToken is empty:', body.error);
            }

            if (typeof body.result !== "undefined") {
                this.accessToken = body.result.access_token;
                this.expiresIn = body.result.expires_in;

                this.headers = {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${this.accessToken}`
                };
                console.log(this.accessToken);
            }
        } catch (e) {
            console.error('auth() error:', e.message);
        }
        return false;
    };

    async getTicker() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'public/ticker';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
                timeout: 5000,
            });
            const body = await res.json();

            if (typeof body.error !== "undefined") {
                console.error('getTicker() error #2:', body.error);
                this.accessToken = '';
            }

            this.lastPrice = parseFloat(body.result.last_price);
            this.indexPrice = parseFloat(body.result.index_price);
        } catch (e) {
            console.error('getTicker() error #1:', e.message);
            this.accessToken = '';
        }

        return false;
    };

    async getTickerHedge() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'public/ticker';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-27DEC19`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });
            const body = await res.json();

            if (typeof body.error !== "undefined") {
                console.log(body.error);
            }

            this.lastPriceHedge = parseFloat(body.result.last_price);
        } catch (e) {
            console.error('getTicker() error:', e.message);
        }

        return false;
    };

    async getOrdersHistory() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/get_user_trades_by_instrument';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&count=150`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const body = await res.json();
            body.result.trades.forEach(function (dataset) {
                console.log(dataset.state, dataset.direction, dataset.amount);
            })


        } catch (e) {
            console.error('getOrdersHistory() error #2:', e.message, 'accessToken:', this.accessToken);
            this.accessToken = '';
        }
        return false;
    };

    async getPosition() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/get_position';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
                timeout: 5000,
            });

            this.activePosition = await res.json();

            if (typeof this.activePosition.error !== "undefined") {
                console.log('getPosition() error #1:', this.activePosition.error);
                if (this.activePosition.error.code === 13004) {
                    this.accessToken = '';
                }
            }

        } catch (e) {
            console.error('getPosition() error #2:', e.message, 'accessToken:', this.accessToken);
            this.accessToken = '';
        }
        return false;
    };

    async getHedgePosition() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/get_position';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-27DEC19`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const body = await res.json();

            if (typeof body.error !== "undefined") {
                console.log('getHedgePosition() error #1:', body.error);
                // if (this.resJson.error.code === 13004) {
                    // this.accessToken = '';
                // }
            }

            if (typeof body.result !== "undefined") {
                this.hedgeActivePosition = Object.assign({}, body);
            }
        } catch (e) {
            console.error('getHedgePosition() error #2:', e.message, 'accessToken:', this.accessToken);
            this.accessToken = '';
        }

        return false;
    };

    async getAccountSummary() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/get_account_summary';
            const params = `?currency=${process.env.D_INSTRUMENT_NAME}`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
                timeout: 5000,
            });

            const body = await res.json();
            this.equity = parseFloat(body.result.equity);
            if (typeof body.error !== "undefined") {
                console.log('getAccountSummary() error #1:', body.error);
                this.accessToken = '';
            }

        } catch (e) {
            console.error('getAccountSummary() error #2:', e.message);
            this.accessToken = '';
        }

        return false;
    };

    async cancelOrders(type = 'all') {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/cancel_all_by_currency';
            const params = `?currency=${process.env.D_INSTRUMENT_NAME}&type=${type}&kind=future`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const body = await res.json();

            if (typeof body.error !== "undefined") {
                console.log('cancelOrders() error #1:', body.error);
                this.accessToken = '';
            }

        } catch (e) {
            console.error('cancelOrders() error #2:', e.message);
            this.accessToken = '';
        }

        return false;
    };

    async cancelOrdersByInstrument(instrument, type = 'all') {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/cancel_all_by_instrument';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-${instrument}&type=${type}`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const body = await res.json();

            if (typeof body.error !== "undefined") {
                console.log('cancelOrdersByInstrument() error #1:', body.error);
                this.accessToken = '';
            }

        } catch (e) {
            console.error('cancelOrdersByInstrument() error #2:', e.message);
            this.accessToken = '';
        }

        return false;
    };

    async cancelOrderOne(orderId) {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/cancel';
            const params = `?order_id=${orderId}`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const body = await res.json();

            if (typeof body.error !== "undefined") {
                console.log('cancelOrderOne() error #1:', body.error);
                // await this.setOrders();
                // this.accessToken = '';
                return false;
            }
            return body.result.order_state;
        } catch (e) {
            console.error('cancelOrderOne() error #2:', e.message);
            // await this.setOrders();
            // this.accessToken = '';
            return false;
        }
    };

    async setBuyLimit(orderSize, orderPrice) {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/buy';
            const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=limit&price=${orderPrice}&post_only=true`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });
            const result = await res.json();

            if (typeof result.error !== "undefined") {
                console.log('setBuyLimit() error #1:', result.error);
                if (result.error.code !== 10009) {
                    this.accessToken = '';
                } else {
                    if (this.activePosition.result.direction === 'zero') {
                        this.errorSellBuy = 1;
                        await this.cancelOrders();
                        const restapi1 = new Restapi();
                        const res0 = await restapi1.emptySocks();
                        console.log('emptySocks() #5:', res0);
                        const resConIp1 = await restapi1.getIpContainer();
                        const res1 = await restapi1.setUserFreezeButton(0, 0);
                        console.log('10009 setUserFreezeButton():', res1);
                        await this.stopContainer(resConIp1);
                    } else {
                        this.flagActRobotPause = 1;
                    }
                }
            }

            // console.log(result);
        } catch (e) {
            console.error('setBuyLimit() error #2:', e.message);
            this.accessToken = '';
        }

        return false;
    };

    async setBuyLimitOne(orderSize, orderPrice) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/buy';
        const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=limit&price=${orderPrice}&post_only=true`;
        const res = await this.fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.lastBuyOrderId = '';
        const res1 = await res.json();
        this.lastBuyOrderId = res1.result.order.order_id;

        return false;
    };

    async setBuyStopLoss(orderSize, orderPrice) {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/buy';
            const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=stop_market&trigger=last_price&stop_price=${orderPrice}&reduce_only=true`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const result =  await res.json();

            if (typeof result.error !== "undefined") {
                console.log('setBuyStopLoss() error #1:', result.error);
            }

        } catch (e) {
            console.error('setBuyStopLoss() error #2:', e.message);
        }

        return false;
    };

    async setSellLimit(orderSize, orderPrice) {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/sell';
            const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=limit&price=${orderPrice}&post_only=true`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const result = await res.json();

            if (typeof result.error !== "undefined") {
                console.log('setSellLimit() error #1:', result.error);
                if (result.error.code !== 10009) {
                    this.accessToken = '';
                } else {
                    if (this.activePosition.result.direction === 'zero') {
                        this.errorSellBuy = 1;
                        await this.cancelOrders();
                        const restapi1 = new Restapi();
                        const res0 = await restapi1.emptySocks();
                        console.log('emptySocks() #6:', res0);
                        const resConIp1 = await restapi1.getIpContainer();
                        const res1 = await restapi1.setUserFreezeButton(0, 0);
                        console.log('10009 setUserFreezeButton():', res1);
                        await this.stopContainer(resConIp1);
                    } else {
                        this.flagActRobotPause = 1;
                    }
                }
            }

            // console.log(result);
        } catch (e) {
            console.error('setSellLimit() error #2:', e.message);
            this.accessToken = '';
        }

        return false;
    };

    async setSellLimitOne(orderSize, orderPrice) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/sell';
        const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=limit&price=${orderPrice}&post_only=true`;
        const res = await this.fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.lastSellOrderId = '';
        const res1 = await res.json();
        this.lastSellOrderId = res1.result.order.order_id;

        return false;
    };

    async setSellStopLoss(orderSize, orderPrice) {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/sell';
            const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=stop_market&trigger=last_price&stop_price=${orderPrice}&reduce_only=true`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const result = await res.json();

            if (typeof result.error !== "undefined") {
                console.log('setSellStopLoss() error #1:', result.error);
            }

        } catch (e) {
            console.error('setSellStopLoss() error #2:', e.message);
        }

        return false;
    };

    async hedgeBuy(size) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/buy';
        const params = `?amount=${size}&instrument_name=${process.env.D_INSTRUMENT_NAME}-27DEC19&type=market`;
        const res = await this.fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.hedgeBuyObj = await res.json();

        return false;
    };

    async hedgeSell(size) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/sell';
        const params = `?amount=${size}&instrument_name=${process.env.D_INSTRUMENT_NAME}-27DEC19&type=market`;
        const res = await this.fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.hedgeSellObj = await res.json();

        return false;
    };

    async marketPerpetaulBuy(size) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/buy';
        const params = `?amount=${size}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=market`;
        const res = await this.fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.hedgeBuyObj = await res.json();

        return false;
    }

    async marketPerpetaulSell(size) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/sell';
        const params = `?amount=${size}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=market`;
        const res = await this.fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.hedgeSellObj = await res.json();

        return false;
    };

    async getOpenOrders() {
        this.amountBuyDirection = 0;
        this.amountSellDirection = 0;

        this.amountBuyStopDirection = 0;
        this.amountSellStopDirection = 0;

        this.openOrdersLimitBuyIds = [];
        this.openOrdersLimitSellIds = [];

        this.openOrdersStoplossBuyIds = [];
        this.openOrdersStoplossSellIds = [];

        this.openOrdersBuys = [];
        this.openOrdersSells = [];

        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/get_open_orders_by_instrument';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL`;
            const res = await this.fetch(`${url}${method}${params}`, {
                headers: this.headers,
                timeout: 5000,
            });

            const len = await res.json();

            if (typeof len.error !== "undefined") {
                this.accessToken = '';
                console.log('getOrders() error #1:', len.error);
            }

            if (typeof len.result !== "undefined") {
                if (len.result.length !== 0) {
                    len.result.forEach((dataset) => {
                        if (dataset.direction === 'buy' && dataset.order_type === 'limit') {
                            this.amountBuyDirection += 1;
                            this.openOrdersLimitBuyIds.push(dataset.order_id);
                            const obj = {};
                            obj.Size = dataset.amount;
                            obj.Price = dataset.price;
                            obj.Id = dataset.order_id;
                            this.openOrdersBuys.push(obj);
                        }
                        if (dataset.direction === 'sell' && dataset.order_type === 'limit') {
                            this.amountSellDirection += 1;
                            this.openOrdersLimitSellIds.push(dataset.order_id);
                            const obj = {};
                            obj.Size = dataset.amount;
                            obj.Price = dataset.price;
                            obj.Id = dataset.order_id;
                            this.openOrdersSells.push(obj);
                        }

                        if (dataset.direction === 'buy' && dataset.order_type === 'stop_market') {
                            this.amountBuyStopDirection += 1;
                            const obj = {};
                            obj.Size = dataset.amount;
                            obj.Price = dataset.stop_price;
                            obj.Id = dataset.order_id;
                            this.openOrdersStoplossBuyIds.push(obj);
                        }
                        if (dataset.direction === 'sell' && dataset.order_type === 'stop_market') {
                            this.amountSellStopDirection += 1;
                            const obj = {};
                            obj.Size = dataset.amount;
                            obj.Price = dataset.stop_price;
                            obj.Id = dataset.order_id;
                            this.openOrdersStoplossSellIds.push(obj);
                        }
                    });
                }
            }

            // For Test. Need make .env dev
            // console.log('Buy', this.amountBuyDirection);
            // console.log('Sell', this.amountSellDirection);
            this.amount = len.result.length;
        } catch (e) {
            this.accessToken = '';
            console.error('getOrders() error #2:', e.message);
        }
        return false;
    };

    async checkAuth(type) {
        if (type === 'just-check') {
            this.counterForExpiresInRe = 0;
            this.counterForExpiresIn += this.time * 0.3;
        }

        if (this.counterForExpiresIn >= 800) {
            this.counterForExpiresIn = 0;
            await this.auth();
        }

        if (type === 're-auth') {
            this.counterForExpiresIn = 0;
            this.counterForExpiresInRe += this.time * 0.5;
        }

        if (this.counterForExpiresInRe >= 800) {
            this.counterForExpiresInRe = 0;
            await this.auth();
        }

        if (type === 'redis-check' || type === 'again') {
            this.counterForExpiresIn = 0;
            this.counterForExpiresInRe = 0;
            await this.auth();
        }

        return false;
    };

    async restartContainer(ip) {
        const body = { t: 1 };

        const headersData = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        const url = `http://${ip}:4243/containers/`;
        const params = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}/`;
        const method = 'restart';
        const res = await fetch2(`${url}${params}${method}`, {
            method: 'post',
            body: 't=2',
            headers: headersData,
        });
        await res.text();
        console.log('Container was restarted!', res.status);

        return false;
    };

    async stopContainer(ip) {
        const body = { t: 1 };

        const headersData = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        const url = `http://${ip}:4243/containers/`;
        const params = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}/`;
        const method = 'stop';
        const res = await fetch2(`${url}${params}${method}`, {
            method: 'post',
            body: 't=2',
            headers: headersData,
        });
        await res.text();
        console.log('Container was stopped!', res.status);

        return false;
    };

    async restartContainerNew(ip) {
        const headersData = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        const url = `http://${ip}:4243/containers/`;
        const params = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}/`;
        const method = 'restart';
        const res = await fetch2(`${url}${params}${method}`, {
            method: 'post',
            body: 't=2',
            headers: headersData,
        });
        console.log(res.status);
        await res.text();

        return false;
    };

    async setGrids() {
        if (this.activePosition.result.direction === 'zero') {
            await this.getOpenOrders();
            await this.getTicker();
            if (
                this.amountBuyDirection < this.gridOrders  ||
                this.amountSellDirection < this.gridOrders ||
                this.gridsTypes === 0 || // VERY IMPORTANT!!!
                this.getGridSettings === 1
            ) {
                if (this.flagActRobotPause === 0) {
                    if (this.flagGetActions === 0 || this.getGridSettings === 1 || this.longShort !== 0) {
                        // this.flagGetActions = 1;

                        if (
                            (this.oldLastPrice < this.lastPrice && this.longShort === 1) || // buy
                            (this.oldLastPrice > this.lastPrice && this.longShort === 2) || // sell
                            (this.longShort === 0) ||
                            (this.getGridSettings === 1) ||
                            (this.flagGetActions === 0)
                        ) {
                            if (this.getGridSettings === 1) {
                                const resultsGetFromRedis = await this.getRedis();
                                console.log(resultsGetFromRedis);
                                if (resultsGetFromRedis[2][1] !== null) {
                                    const foo = JSON.parse(resultsGetFromRedis[2][1]);
                                    const { gridsTypes } = foo;
                                    this.gridsTypes = parseFloat(gridsTypes);
                                    if (this.gridsTypes === 0) {
                                        const {
                                            gridOrders, gridStep, stepMultiplier, gridOrderSize,
                                            sizeMultiplier, customTake, longShort,
                                            stepMultiplierSign, sizeMultiplierSign,
                                            gridOrderSizePer, firstOrderType, stpStep,
                                            sideLevelLow, sideLevelHigh
                                        } = foo;
                                        this.gridOrders = parseFloat(gridOrders);
                                        this.gridStep = parseFloat(gridStep);
                                        this.stepMultiplier = parseFloat(stepMultiplier);
                                        this.gridOrderSize = parseFloat(gridOrderSize);
                                        this.gridOrderSizePer = parseFloat(gridOrderSizePer);
                                        this.sizeMultiplier = parseFloat(sizeMultiplier);
                                        this.customTake = parseFloat(customTake);
                                        this.longShort = parseFloat(longShort);
                                        this.stepMultiplierSign = parseFloat(stepMultiplierSign);
                                        this.sizeMultiplierSign = parseFloat(sizeMultiplierSign);
                                        this.stoplossStep = parseFloat(stpStep);
                                        this.gridFirstOrder = parseFloat(firstOrderType);
                                        this.sideLevelLow = parseFloat(sideLevelLow);
                                        this.sideLevelHigh = parseFloat(sideLevelHigh);

                                        if (this.sideLevelLow !== 0 && this.sideLevelHigh !== 0) {
                                            await this.getTicker();
                                            if (this.lastPrice >= this.sideLevelLow && this.lastPrice < this.sideLevelHigh) {
                                                this.longShort = 0;
                                            }

                                            if (this.lastPrice < this.sideLevelLow) {
                                                this.longShort = 1;
                                            }

                                            if (this.lastPrice >= this.sideLevelHigh) {
                                                this.longShort = 2;
                                            }
                                        }
                                    }

                                    if (this.gridsTypes === 1) {
                                        const {
                                            customGridsBuy, customGridsSell,
                                            customGridsBuySlPrice, customGridsBuySlSize,
                                            customGridsSellSlPrice, customGridsSellSlSize
                                        } = foo;
                                        console.log('typeofS:', typeof customGridsBuy, typeof customGridsSell);
                                        console.log('foo', foo);
                                        this.customGridsBuy = [];
                                        try {
                                            if (typeof customGridsBuy !== "undefined" && customGridsBuy !== '0') {
                                                this.customGridsBuy = JSON.parse(customGridsBuy).map(value => {
                                                    return value;
                                                });
                                                // this.customGridsBuy.reverse();
                                            }
                                        } catch (err) {
                                            console.log('JSON.parse(customGridsBuy)', err.message)
                                            this.customGridsBuy = [];
                                        }

                                        this.customGridsSell = [];
                                        try {
                                            if (typeof customGridsSell !== "undefined" && customGridsSell !== '0') {
                                                this.customGridsSell = JSON.parse(customGridsSell).map(value => {
                                                    return value;
                                                });
                                                // this.customGridsSell.reverse();
                                            }
                                        } catch (err) {
                                            console.log('JSON.parse(customGridsSell)', err.message)
                                            this.customGridsSell = [];
                                        }

                                        if (this.customGridsBuy.length !== 0 && this.customGridsSell.length !== 0) {
                                            this.longShort = 0;
                                        }

                                        if (this.customGridsBuy.length !== 0 && this.customGridsSell.length === 0) {
                                            this.longShort = 1;
                                        }

                                        if (this.customGridsBuy.length === 0 && this.customGridsSell.length !== 0) {
                                            this.longShort = 2;
                                        }

                                        this.customGridsBuySlPrice = parseFloat(customGridsBuySlPrice);
                                        this.customGridsSellSlPrice = parseFloat(customGridsSellSlPrice);

                                        this.customGridsBuySlSize = parseFloat(customGridsBuySlSize);
                                        this.customGridsSellSlSize = parseFloat(customGridsSellSlSize);
                                    }
                                }
                                console.log(this.gridOrders);
                                this.getGridSettings = 0;
                            }

                            this.flagGetActions = 1;
                            console.log('oldLastPrice:', this.oldLastPrice, 'lastPrice:', this.lastPrice);
                            this.oldLastPrice = this.lastPrice;

                            this.gridBuy = [];
                            if (this.longShort === 0 || this.longShort === 1) {
                                let gridStepBuy = this.gridStep;
                                let gridOrderSizeBuy = 0;
                                if (this.gridFirstOrder === 0) {
                                    gridOrderSizeBuy = this.gridOrderSize;
                                }
                                if (this.gridFirstOrder === 1) {
                                    await this.getAccountSummary();
                                    gridOrderSizeBuy = Math.floor((this.indexPrice * this.equity) / 100 * this.gridOrderSizePer);
                                    console.log(this.indexPrice, this.indexPrice, this.gridOrderSizePer, typeof this.gridOrderSizePer);
                                    gridOrderSizeBuy = Math.round(gridOrderSizeBuy / 10) * 10;
                                    if (gridOrderSizeBuy === 0) gridOrderSizeBuy = 10;
                                }
                                console.log(gridOrderSizeBuy);
                                // gridOrderSizeBuy = this.gridOrderSize;

                                let objBuy = {};
                                if (this.gridsTypes === 0) {
                                    objBuy.Price = this.lastPrice - gridStepBuy;
                                    objBuy.Size = gridOrderSizeBuy;
                                }
                                if (this.gridsTypes === 1 && this.customGridsBuy.length !== 0) {
                                    objBuy.Price = this.lastPrice - parseFloat(this.customGridsBuy[0][1]);
                                    objBuy.Size = parseFloat(this.customGridsBuy[0][0]);
                                }
                                this.gridBuy.push(objBuy);
                                this.checkSizes = [];
                                this.checkSizesBuy = [];
                                this.middlePriceBuy = 0;
                                this.countSizeBuy = 0;
                                this.lastBuyOrderPrice = 0;
                                this.checkSizes.push(objBuy.Size);
                                this.checkSizesBuy.push(objBuy.Size);
                                this.middlePriceBuy = objBuy.Size * objBuy.Price;
                                this.countSizeBuy = objBuy.Size;

                                // console.log('First time:', gridStepBuy, this.stepMultiplier);
                                if (this.gridsTypes === 0) {
                                    for (let i = 0; i <= this.gridOrders - 2; i++) {
                                        let obj = {};
                                        if (this.stepMultiplierSign === 0) gridStepBuy = gridStepBuy * this.stepMultiplier;
                                        // if (this.stepMultiplierSign === 1) gridStepBuy = gridStepBuy + this.stepMultiplier;
                                        // if (this.stepMultiplierSign === 2) gridStepBuy = gridStepBuy + this.stepMultiplier * (i + 1);
                                        if (this.stepMultiplierSign === 1) gridStepBuy = this.stepMultiplier;
                                        if (this.stepMultiplierSign === 2) gridStepBuy = this.stepMultiplier * (i + 1);

                                        obj.Price = this.gridBuy[i].Price - gridStepBuy;
                                        // console.log('Next time:', gridStepBuy, this.stepMultiplier);
                                        if (this.sizeMultiplierSign === 0) gridOrderSizeBuy = gridOrderSizeBuy * this.sizeMultiplier;
                                        if (this.sizeMultiplierSign === 1) gridOrderSizeBuy = gridOrderSizeBuy + this.sizeMultiplier;
                                        if (this.sizeMultiplierSign === 2) gridOrderSizeBuy = gridOrderSizeBuy + this.sizeMultiplier * (i + 1);

                                        obj.Size = gridOrderSizeBuy;
                                        this.gridBuy.push(obj);
                                        this.checkSizes.push(this.checkSizes[i] + obj.Size);
                                        this.middlePriceBuy += obj.Size * obj.Price;
                                        this.countSizeBuy += obj.Size;
                                    }
                                }

                                if (this.gridsTypes === 1 && this.customGridsBuy.length >= 2) {
                                    for (let i = 0; i <= this.customGridsBuy.length - 2; i++) {
                                        let obj = {};
                                        obj.Price = this.gridBuy[i].Price - parseFloat(this.customGridsBuy[i + 1][1]);
                                        obj.Size = parseFloat(this.customGridsBuy[i + 1][0]);
                                        this.gridBuy.push(obj);

                                        this.checkSizesBuy.push(this.checkSizesBuy[i] + obj.Size);
                                        this.middlePriceBuy += obj.Size * obj.Price;
                                        this.countSizeBuy += obj.Size;
                                    }
                                }

                                if (this.gridsTypes === 0) {
                                    this.lastBuyOrderPrice = this.gridBuy[this.gridBuy.length - 1].Price - this.stoplossStep;
                                }

                                if (this.gridsTypes === 1 && this.customGridsBuy.length !== 0) {
                                    this.lastBuyOrderPrice = this.gridBuy[this.gridBuy.length - 1].Price - this.customGridsBuySlPrice;
                                }

                                console.log('checkSizes', this.checkSizes);
                                console.log('checkSizesBuy', this.checkSizesBuy);
                                console.log('gridOrders', this.gridOrders);
                                console.log('gridBuy', this.gridBuy);
                                console.log('lastBuyOrderPrice', this.lastBuyOrderPrice);
                                console.log('this.customGridsBuy', this.customGridsBuy);
                                console.log('this.customGridsBuySlPrice', this.customGridsBuySlPrice, typeof this.customGridsBuySlPrice);
                            }

                            this.gridSell = [];
                            if (this.longShort === 0 || this.longShort === 2) {
                                let gridStepSell = this.gridStep;
                                let gridOrderSizeSell = 0;
                                if (this.gridFirstOrder === 0) {
                                    gridOrderSizeSell = this.gridOrderSize;
                                }
                                if (this.gridFirstOrder === 1) {
                                    await this.getAccountSummary();
                                    gridOrderSizeSell = Math.floor((this.indexPrice * this.equity) / 100 * this.gridOrderSizePer);
                                    gridOrderSizeSell = Math.round(gridOrderSizeSell / 10) * 10;
                                    if (gridOrderSizeSell === 0) gridOrderSizeSell = 10;
                                }

                                this.middlePriceSell = 0;
                                this.countSizeSell = 0;
                                this.lastSellOrderPrice = 0;
                                let objSell = {};
                                if (this.gridsTypes === 0) {
                                    objSell.Price = this.lastPrice + gridStepSell;
                                    objSell.Size = gridOrderSizeSell;
                                }

                                if (this.gridsTypes === 1 && this.customGridsSell.length !== 0) {
                                    objSell.Price = this.lastPrice + parseFloat(this.customGridsSell[0][1]);
                                    objSell.Size = parseFloat(this.customGridsSell[0][0]);
                                }

                                this.gridSell.push(objSell);
                                console.log('this.gridSell', this.gridSell);
                                this.checkSizes = [];
                                this.checkSizesSell = [];
                                this.checkSizes.push(objSell.Size);
                                this.checkSizesSell.push(objSell.Size);
                                this.middlePriceSell = objSell.Size * objSell.Price;
                                this.countSizeSell = objSell.Size;

                                if (this.gridsTypes === 0) {
                                    for (let i = 0; i <= this.gridOrders - 2; i++) {
                                        let obj = {};
                                        if (this.stepMultiplierSign === 0) gridStepSell = gridStepSell * this.stepMultiplier;
                                        // if (this.stepMultiplierSign === 1) gridStepSell = gridStepSell + this.stepMultiplier;
                                        // if (this.stepMultiplierSign === 2) gridStepSell = gridStepSell + this.stepMultiplier * (i + 1);
                                        if (this.stepMultiplierSign === 1) gridStepSell = this.stepMultiplier;
                                        if (this.stepMultiplierSign === 2) gridStepSell = this.stepMultiplier * (i + 1);


                                        obj.Price = this.gridSell[i].Price + gridStepSell;
                                        if (this.sizeMultiplierSign === 0) gridOrderSizeSell = gridOrderSizeSell * this.sizeMultiplier;
                                        if (this.sizeMultiplierSign === 1) gridOrderSizeSell = gridOrderSizeSell + this.sizeMultiplier;
                                        if (this.sizeMultiplierSign === 2) gridOrderSizeSell = gridOrderSizeSell + this.sizeMultiplier * (i + 1);

                                        obj.Size = gridOrderSizeSell;
                                        this.gridSell.push(obj);
                                        this.checkSizes.push(this.checkSizes[i] + obj.Size);
                                        this.middlePriceSell += obj.Size * obj.Price;
                                        this.countSizeSell += obj.Size;
                                    }
                                }

                                if (this.gridsTypes === 1 && this.customGridsSell.length >= 2) {
                                    for (let i = 0; i <= this.customGridsSell.length - 2; i++) {
                                        let obj = {};
                                        obj.Price = this.gridSell[i].Price + parseFloat(this.customGridsSell[i + 1][1]);
                                        obj.Size = parseFloat(this.customGridsSell[i + 1][0]);
                                        this.gridSell.push(obj);

                                        this.checkSizesSell.push(this.checkSizesSell[i] + obj.Size);
                                        this.middlePriceBuy += obj.Size * obj.Price;
                                        this.countSizeBuy += obj.Size;
                                    }
                                }

                                if (this.gridsTypes === 0) {
                                    this.lastSellOrderPrice = this.gridSell[this.gridSell.length - 1].Price + this.stoplossStep;
                                }

                                if (this.gridsTypes === 1 && this.customGridsSell.length !== 0) {
                                    this.lastSellOrderPrice = this.gridSell[this.gridSell.length - 1].Price + this.customGridsSellSlPrice;
                                }

                                console.log('checkSizes', this.checkSizes);
                                console.log('checkSizesSell', this.checkSizesSell);
                                console.log('gridSell', this.gridSell);
                            }

                            console.log('cancelOrdersByInstrument():', this.longShort);
                            await this.cancelOrdersByInstrument('PERPETUAL');
                            await this.setOrders();
                        }
                    }
                } else {
                    this.globalStopFlag = 1;
                    console.log('Direction is zero #2.');
                    this.redisPub.publish('start', `s_${process.env.D_USER_ID}`);
                    console.log('Robot was stoped #2.');
                    let resConIp = '';
                    const restapi = new Restapi();
                    const res0 = await restapi.emptySocks();
                    console.log('emptySocks() #4:', res0);
                    resConIp = await restapi.getIpContainer();
                    console.log('getIpContainer():', resConIp);
                    const res1 = await restapi.setUserFreezeButton(0, 0);
                    console.log('setUserFreezeButton():', res1);
                    const res2 = await restapi.setPauseRobot(0);
                    console.log('setPauseRobot():', res2);
                    await this.cancelOrdersByInstrument('PERPETUAL');
                    await this.stopContainer(resConIp);
                }
            }
        }

        if (this.activePosition.result.direction === 'buy') {
            this.flagGetActions = 0;
            // Return new array where all prices LESS lastPrice.
            // It is same that foreach and DELETE from array prices MORE lastprice
            await this.getOpenOrders();

            this.gridBuy = [];
            this.openOrdersBuys.forEach((dataset) => {
                this.gridBuy.push(dataset);
            });
            /*
            let pos = this.gridOrders - this.amountBuyDirection;
            if ((this.gridBuy.length + pos) !== this.gridOrders) {
                let num = pos - (this.gridOrders - this.gridBuy.length);
                this.gridBuy = this.gridBuy.slice(num);
            }
            */
            // console.log('BUYS #1 pos:', this.openOrdersBuys);
            console.log('BUYS #2 gridBuy:', this.gridBuy);

            this.gridSell = [];
            if (this.getGridSettings === 1) {
                const resultsGetFromRedis = await this.getRedis();
                console.log(resultsGetFromRedis);
                if (resultsGetFromRedis[2][1] !== null) {
                    const foo = JSON.parse(resultsGetFromRedis[2][1]);
                    const { gridsTypes } = foo;
                    this.gridsTypes = parseFloat(gridsTypes);
                    if (this.gridsTypes === 0) {
                        const {
                            gridOrders, gridStep, stepMultiplier, gridOrderSize,
                            sizeMultiplier, customTake, longShort,
                            stepMultiplierSign, sizeMultiplierSign,
                            gridOrderSizePer, firstOrderType, stpStep,
                            sideLevelLow, sideLevelHigh
                        } = foo;
                        this.gridOrders = parseFloat(gridOrders);
                        this.gridStep = parseFloat(gridStep);
                        this.stepMultiplier = parseFloat(stepMultiplier);
                        this.gridOrderSize = parseFloat(gridOrderSize);
                        this.gridOrderSizePer = parseFloat(gridOrderSizePer);
                        this.sizeMultiplier = parseFloat(sizeMultiplier);
                        this.customTake = parseFloat(customTake);
                        this.longShort = parseFloat(longShort);
                        this.stepMultiplierSign = parseFloat(stepMultiplierSign);
                        this.sizeMultiplierSign = parseFloat(sizeMultiplierSign);
                        this.stoplossStep = parseFloat(stpStep);
                        this.gridFirstOrder = parseFloat(firstOrderType);
                        this.sideLevelLow = parseFloat(sideLevelLow);
                        this.sideLevelHigh = parseFloat(sideLevelHigh);

                        if (this.sideLevelLow !== 0 && this.sideLevelHigh !== 0) {
                            await this.getTicker();
                            if (this.lastPrice >= this.sideLevelLow && this.lastPrice < this.sideLevelHigh) {
                                this.longShort = 0;
                            }

                            if (this.lastPrice < this.sideLevelLow) {
                                this.longShort = 1;
                            }

                            if (this.lastPrice >= this.sideLevelHigh) {
                                this.longShort = 2;
                            }
                        }
                    }

                    if (this.gridsTypes === 1) {
                        const {
                            customGridsBuy, customGridsSell,
                            customGridsBuySlPrice, customGridsBuySlSize,
                            customGridsSellSlPrice, customGridsSellSlSize
                        } = foo;
                        console.log('typeofS:', typeof customGridsBuy, typeof customGridsSell);
                        this.customGridsBuy = [];
                        try {
                            if (typeof customGridsBuy !== "undefined" && customGridsBuy !== '0') {
                                this.customGridsBuy = JSON.parse(customGridsBuy).map(value => {
                                    return value;
                                });
                                // this.customGridsBuy.reverse();
                            }
                        } catch (err) {
                            console.log('JSON.parse(customGridsBuy)', err.message)
                            this.customGridsBuy = [];
                        }

                        this.customGridsSell = [];
                        try {
                            if (typeof customGridsSell !== "undefined" && customGridsSell !== '0') {
                                this.customGridsSell = JSON.parse(customGridsSell).map(value => {
                                    return value;
                                });
                                // this.customGridsSell.reverse();
                            }
                        } catch (err) {
                            console.log('JSON.parse(customGridsSell)', err.message)
                            this.customGridsSell = [];
                        }

                        if (this.customGridsBuy.length !== 0 && this.customGridsSell.length !== 0) {
                            this.longShort = 0;
                        }

                        if (this.customGridsBuy.length !== 0 && this.customGridsSell.length === 0) {
                            this.longShort = 1;
                        }

                        if (this.customGridsBuy.length === 0 && this.customGridsSell.length !== 0) {
                            this.longShort = 2;
                        }

                        this.customGridsBuySlPrice = parseFloat(customGridsBuySlPrice);
                        this.customGridsSellSlPrice = parseFloat(customGridsSellSlPrice);

                        this.customGridsBuySlSize = parseFloat(customGridsBuySlSize);
                        this.customGridsSellSlSize = parseFloat(customGridsSellSlSize)
                    }
                }
                this.getGridSettings = 0;
            }

            let gridStepSell = this.gridStep;
            let gridOrderSizeSell = 0;
            if (this.gridFirstOrder === 0) {
                gridOrderSizeSell = this.gridOrderSize;
            }
            if (this.gridFirstOrder === 1) {
                await this.getAccountSummary();
                gridOrderSizeSell = Math.floor((this.indexPrice * this.equity) / 100 * this.gridOrderSizePer);
                gridOrderSizeSell = Math.round(gridOrderSizeSell / 10) * 10;
                if (gridOrderSizeSell === 0) gridOrderSizeSell = 10;
            }

            let objSell = {};
            // objSell.Price = this.activePosition.result.average_price + gridStepSell;
            // if (this.customTake <= 0) {
                // this.customTake = gridStepSell;
            // }
            // console.log(this.customTake);

            if (this.gridsTypes === 0) {
                objSell.Price = this.activePosition.result.average_price + this.customTake;
                objSell.Size = Math.abs(this.activePosition.result.size);
            }

            if (this.gridsTypes === 1 && this.customGridsBuy.length !== 0) {
                const indexBuy = this.customGridsBuy.length - this.amountBuyDirection - 1;
                if (indexBuy >= 0) {
                    objSell.Price = this.activePosition.result.average_price + parseFloat(this.customGridsBuy[indexBuy][2]);
                } else {
                    objSell.Price = this.activePosition.result.average_price + parseFloat(this.customGridsBuy[0][1]);
                }
                objSell.Size = Math.abs(this.activePosition.result.size);
            }

            this.gridSell.push(objSell);

            if (this.flagActRobotPause === 0 && this.longShort === 0) {
                this.checkSizes = [];
                this.checkSizesSell = [];

                if (this.gridsTypes === 0) {
                    for (let i = 0; i <= this.gridOrders - 1; i++) {
                        let obj = {};
                        obj.Price = i === 0 ? this.gridSell[i].Price + 0 : this.gridSell[i].Price + gridStepSell;
                        if (this.stepMultiplierSign === 0) gridStepSell = gridStepSell * this.stepMultiplier;
                        // if (this.stepMultiplierSign === 1) gridStepSell = gridStepSell + this.stepMultiplier;
                        // if (this.stepMultiplierSign === 2) gridStepSell = gridStepSell + this.stepMultiplier * (i + 1);
                        if (this.stepMultiplierSign === 1) gridStepSell = this.stepMultiplier;
                        if (this.stepMultiplierSign === 2) gridStepSell = this.stepMultiplier * (i + 1);

                        obj.Size = gridOrderSizeSell;
                        if (this.sizeMultiplierSign === 0) gridOrderSizeSell = gridOrderSizeSell * this.sizeMultiplier;
                        if (this.sizeMultiplierSign === 1) gridOrderSizeSell = gridOrderSizeSell + this.sizeMultiplier;
                        if (this.sizeMultiplierSign === 2) gridOrderSizeSell = gridOrderSizeSell + this.sizeMultiplier * (i + 1);

                        this.gridSell.push(obj);
                        if (i === 0) {
                            this.checkSizes.push(obj.Size);
                        } else {
                            this.checkSizes.push(this.checkSizes[i - 1] + obj.Size);
                        }
                    }
                }

                if (this.gridsTypes === 1 && this.customGridsSell.length >= 1) {
                    for (let i = 0; i <= this.customGridsSell.length - 1; i++) {
                        let obj = {};
                        obj.Price = i === 0 ? this.gridSell[i].Price + 0 : this.gridSell[i].Price + parseFloat(this.customGridsSell[i][1]);
                        obj.Size = parseFloat(this.customGridsSell[i][0]);
                        this.gridSell.push(obj);

                        if (i === 0) {
                            this.checkSizesSell.push(obj.Size);
                        } else {
                            this.checkSizesSell.push(this.checkSizesSell[i - 1] + obj.Size);
                        }
                    }
                }

                // this.lastSellOrderPrice = this.gridSell[this.gridSell.length - 1].Price + this.stoplossStep;

                console.log('New Check Sizes', this.checkSizes);
                console.log('New Check Sizes', this.checkSizesSell);
                console.log('BUYS #3 gridSell:', this.gridSell);
            } else {
                console.log('BUYS #4 gridSell, pause:', this.gridSell);
            }
        }

        if (this.activePosition.result.direction === 'sell') {
            this.flagGetActions = 0;
            // Return new array where all prices MORE lastPrice.
            // It is same that foreach and DELETE from array prices LESS lastprice
            await this.getOpenOrders();

            this.gridSell = [];
            this.openOrdersSells.forEach((dataset) => {
                this.gridSell.push(dataset);
            });
            /*
            let pos = this.gridOrders - this.amountSellDirection;
            if ((this.gridSell.length + pos) !== this.gridOrders) {
                let num = pos - (this.gridOrders - this.gridSell.length);
                this.gridSell = this.gridSell.slice(num);
            }
            */
            // console.log('SELLS #1 pos:', this.openOrdersSells);
            console.log('SELLS #2 gridSell:', this.gridSell);

            this.gridBuy = [];

            if (this.getGridSettings === 1) {
                const resultsGetFromRedis = await this.getRedis();
                console.log(resultsGetFromRedis);
                if (resultsGetFromRedis[2][1] !== null) {
                    const foo = JSON.parse(resultsGetFromRedis[2][1]);
                    const { gridsTypes } = foo;
                    this.gridsTypes = parseFloat(gridsTypes);
                    if (this.gridsTypes === 0) {
                        const {
                            gridOrders, gridStep, stepMultiplier, gridOrderSize,
                            sizeMultiplier, customTake, longShort,
                            stepMultiplierSign, sizeMultiplierSign,
                            gridOrderSizePer, firstOrderType, stpStep,
                            sideLevelLow, sideLevelHigh
                        } = foo;
                        this.gridOrders = parseFloat(gridOrders);
                        this.gridStep = parseFloat(gridStep);
                        this.stepMultiplier = parseFloat(stepMultiplier);
                        this.gridOrderSize = parseFloat(gridOrderSize);
                        this.gridOrderSizePer = parseFloat(gridOrderSizePer);
                        this.sizeMultiplier = parseFloat(sizeMultiplier);
                        this.customTake = parseFloat(customTake);
                        this.longShort = parseFloat(longShort);
                        this.stepMultiplierSign = parseFloat(stepMultiplierSign);
                        this.sizeMultiplierSign = parseFloat(sizeMultiplierSign);
                        this.stoplossStep = parseFloat(stpStep);
                        this.gridFirstOrder = parseFloat(firstOrderType);
                        this.sideLevelLow = parseFloat(sideLevelLow);
                        this.sideLevelHigh = parseFloat(sideLevelHigh);

                        if (this.sideLevelLow !== 0 && this.sideLevelHigh !== 0) {
                            await this.getTicker();
                            if (this.lastPrice >= this.sideLevelLow && this.lastPrice < this.sideLevelHigh) {
                                this.longShort = 0;
                            }

                            if (this.lastPrice < this.sideLevelLow) {
                                this.longShort = 1;
                            }

                            if (this.lastPrice >= this.sideLevelHigh) {
                                this.longShort = 2;
                            }
                        }
                    }

                    if (this.gridsTypes === 1) {
                        const {
                            customGridsBuy, customGridsSell,
                            customGridsBuySlPrice, customGridsBuySlSize,
                            customGridsSellSlPrice, customGridsSellSlSize
                        } = foo;

                        console.log('typeofS:', typeof customGridsBuy, typeof customGridsSell);
                        this.customGridsBuy = [];
                        try {
                            if (typeof customGridsBuy !== "undefined" && customGridsBuy !== '0') {
                                this.customGridsBuy = JSON.parse(customGridsBuy).map(value => {
                                    return value;
                                });
                                // this.customGridsBuy.reverse();
                            }
                        } catch (err) {
                            console.log('JSON.parse(customGridsBuy)', err.message)
                            this.customGridsBuy = [];
                        }

                        this.customGridsSell = [];
                        try {
                            if (typeof customGridsSell !== "undefined" && customGridsSell !== '0') {
                                this.customGridsSell = JSON.parse(customGridsSell).map(value => {
                                    return value;
                                });
                                // this.customGridsSell.reverse();
                            }
                        } catch (err) {
                            console.log('JSON.parse(customGridsSell)', err.message)
                            this.customGridsSell = [];
                        }

                        if (this.customGridsBuy.length !== 0 && this.customGridsSell.length !== 0) {
                            this.longShort = 0;
                        }

                        if (this.customGridsBuy.length !== 0 && this.customGridsSell.length === 0) {
                            this.longShort = 1;
                        }

                        if (this.customGridsBuy.length === 0 && this.customGridsSell.length !== 0) {
                            this.longShort = 2;
                        }

                        this.customGridsBuySlPrice = parseFloat(customGridsBuySlPrice);
                        this.customGridsSellSlPrice = parseFloat(customGridsSellSlPrice);

                        this.customGridsBuySlSize = parseFloat(customGridsBuySlSize);
                        this.customGridsSellSlSize = parseFloat(customGridsSellSlSize);
                    }
                }
                this.getGridSettings = 0;
            }

            let gridStepBuy = this.gridStep;
            let gridOrderSizeBuy = 0;
            if (this.gridFirstOrder === 0) {
                gridOrderSizeBuy = this.gridOrderSize;
            }
            if (this.gridFirstOrder === 1) {
                await this.getAccountSummary();
                gridOrderSizeBuy = Math.floor((this.indexPrice * this.equity) / 100 * this.gridOrderSizePer);
                gridOrderSizeBuy = Math.round(gridOrderSizeBuy / 10) * 10;
                if (gridOrderSizeBuy === 0) gridOrderSizeBuy = 10;
            }

            let objBuy = {};
            // objBuy.Price = this.activePosition.result.average_price - gridStepBuy;
            // if (this.customTake <= 0) {
                // this.customTake = gridStepBuy;
            // }


            if (this.gridsTypes === 0) {
                objBuy.Price = this.activePosition.result.average_price - this.customTake;
                objBuy.Size = Math.abs(this.activePosition.result.size);
            }

            if (this.gridsTypes === 1 && this.customGridsSell.length !== 0) {
                const indexSell = this.customGridsSell.length - this.amountSellDirection - 1;
                if (indexSell >= 0) {
                    objBuy.Price = this.activePosition.result.average_price - parseFloat(this.customGridsSell[indexSell][2]);
                } else {
                    objBuy.Price = this.activePosition.result.average_price - parseFloat(this.customGridsSell[0][1]);
                }

                objBuy.Size = Math.abs(this.activePosition.result.size);
            }
            this.gridBuy.push(objBuy);

            if (this.flagActRobotPause === 0 && this.longShort === 0) {
                this.checkSizes = [];
                this.checkSizesBuy = [];

                if (this.gridsTypes === 0) {
                    for (let i = 0; i <= this.gridOrders - 1; i++) {
                        let obj = {};
                        obj.Price = i === 0 ? this.gridBuy[i].Price - 0 : this.gridBuy[i].Price - gridStepBuy;
                        if (this.stepMultiplierSign === 0) gridStepBuy = gridStepBuy * this.stepMultiplier;
                        // if (this.stepMultiplierSign === 1) gridStepBuy = gridStepBuy + this.stepMultiplier;
                        // if (this.stepMultiplierSign === 2) gridStepBuy = gridStepBuy + this.stepMultiplier * (i + 1);
                        if (this.stepMultiplierSign === 1) gridStepBuy = this.stepMultiplier;
                        if (this.stepMultiplierSign === 2) gridStepBuy = this.stepMultiplier * (i + 1);

                        obj.Size = gridOrderSizeBuy;
                        if (this.sizeMultiplierSign === 0) gridOrderSizeBuy = gridOrderSizeBuy * this.sizeMultiplier;
                        if (this.sizeMultiplierSign === 1) gridOrderSizeBuy = gridOrderSizeBuy + this.sizeMultiplier;
                        if (this.sizeMultiplierSign === 2) gridOrderSizeBuy = gridOrderSizeBuy + this.sizeMultiplier * (i + 1);

                        this.gridBuy.push(obj);
                        if (i === 0) {
                            this.checkSizes.push(obj.Size);
                        } else {
                            this.checkSizes.push(this.checkSizes[i - 1] + obj.Size);
                        }
                    }
                }

                if (this.gridsTypes === 1 && this.customGridsBuy.length >= 1) {
                    for (let i = 0; i <= this.customGridsBuy.length - 1; i++) {
                        let obj = {};
                        obj.Price = i === 0 ? this.gridBuy[i].Price - 0 : this.gridBuy[i].Price - parseFloat(this.customGridsBuy[i][1]);
                        obj.Size = parseFloat(this.customGridsBuy[i][0]);
                        this.gridBuy.push(obj);

                        if (i === 0) {
                            this.checkSizesBuy.push(obj.Size);
                        } else {
                            this.checkSizesBuy.push(this.checkSizesBuy[i - 1] + obj.Size);
                        }
                    }
                }

                // this.lastBuyOrderPrice = this.gridBuy[this.gridBuy.length - 1].Price - this.stoplossStep;

                console.log('New Check Sizes', this.checkSizes);
                console.log('New Check Sizes', this.checkSizesBuy);
                console.log('SELLS #3 gridBuy:', this.gridBuy);
            } else {
                console.log('SELLS #4 gridBuy, pause:', this.gridBuy);
            }
        }

        return false;
    };

    async getRedis() {
        return new Promise((resolve, reject) => {
          const id = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}`;
          this.redis.pipeline([
            ['get', `${id}:buys`],
            ['get', `${id}:sells`],
            ['get', `${id}:gridsettings`],
          ]).exec((err, results) => {
            if (err) reject(err);

            resolve(results);
          });
        });
    };

    async delRedis() {
        return new Promise((resolve, reject) => {
          const id = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}`;
          this.redis.pipeline([
            ['del', `${id}:buys`],
            ['del', `${id}:sells`],
          ]).exec((err, results) => {
            if (err) reject(err);

            // console.log('Del result:', results);

            resolve(results);
          });
        });
    };

    async setRedis(msg) {
        return new Promise((resolve, reject) => {
          const id = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}`;
          this.redis.pipeline([
            ['set', `${id}:buys`, JSON.stringify(this.gridBuy)],
            ['set', `${id}:sells`, JSON.stringify(this.gridSell)],
            ['set', `${id}:gridsettings`, msg],
          ]).exec((err, results) => {
            if (err) reject(err);

            resolve(results);
          });
        });
    };

    async keepDataForReboot() {
        return new Promise((resolve, reject) => {
            const id = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}`;
            this.redis.pipeline([
                ['set', `${id}:activePositionOldSize`, JSON.stringify(this.activePositionOldSize)],
                ['set', `${id}:directionOld`, this.directionOld],
            ]).exec((err, results) => {
                if (err) reject(err);

                resolve(results);
            });
        });
    };

    async getDataForReboot() {
        return new Promise((resolve, reject) => {
            const id = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}`;
            this.redis.pipeline([
                ['get', `${id}:activePositionOldSize`],
                ['get', `${id}:directionOld`],
            ]).exec((err, results) => {
                if (err) reject(err);

                resolve(results);
            });
        });
    };

    async removeDataForReboot() {
        return new Promise((resolve, reject) => {
            const id = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}`;
            this.redis.pipeline([
                ['del', `${id}:activePositionOldSize`],
                ['del', `${id}:directionOld`],
            ]).exec((err, results) => {
                if (err) reject(err);

                resolve(results);
            });
        });
    };

    async setOrders() {
        // await this.getOpenOrders();

        console.log(this.hedgeOrStoploss, this.flagActRobotPause, this.activePosition.result.direction);
        // SET LIMIT ORDERS
        // ================== HOME ==================
        // FIRST CONDITION IS HEDGE; SECOND CONDITIONS IS STOPLOSS + PAUSE + NO ZERO
        if (this.hedgeOrStoploss === 1 && this.flagActRobotPause === 1 && this.activePosition.result.direction !== 'zero') {
            await this.getOpenOrders();
            if (this.activePosition.result.direction === 'sell') {
                for (let objBuyO of this.openOrdersBuys) {
                    await this.cancelOrderOne(objBuyO.Id);
                }

                for (let objBuy of this.gridBuy) {
                    await this.setBuyLimit(objBuy.Size, objBuy.Price);
                }

            }

            if (this.activePosition.result.direction === 'buy') {
                for (let objSellO of this.openOrdersSells) {
                    await this.cancelOrderOne(objSellO.Id);
                }

                for (let objSell of this.gridSell) {
                    await this.setSellLimit(objSell.Size, objSell.Price);
                }

            }
        }
        // ================== END ===================
        // SET LIMIT ORDERS


        // SET STOPLOSS ORDERS
        // ================== HOME ==================
        // await this.getOpenOrders();
        if (this.hedgeOrStoploss === 1 && this.activePosition.result.direction === 'zero') {
            const size = this.checkSizes[this.checkSizes.length - 1] + this.gridOrderSize;

            if (this.lastBuyOrderPrice <= 0) this.lastBuyOrderPrice = 0.5;

            if (this.gridsTypes === 0) {
                if (this.longShort === 0 || this.longShort === 1) {
                    await this.setSellStopLoss(size, this.lastBuyOrderPrice);
                }
            }

            if (this.gridsTypes === 1 && this.customGridsBuy.length !== 0 ) {
                if (this.longShort === 0 || this.longShort === 1) {
                    const obj = {};
                    // obj.slSize = this.checkSizesBuy[this.checkSizesBuy.length - 1] + this.customGridsSellSlSize;
                    obj.slSize = this.checkSizesBuy[this.checkSizesBuy.length - 1] * 1;
                    obj.slPrice = this.lastBuyOrderPrice;
                    await this.setSellStopLoss(obj.slSize, obj.slPrice);
                }
            }

            if (this.gridsTypes === 0) {
                if (this.longShort === 0 || this.longShort === 2) {
                    await this.setBuyStopLoss(size, this.lastSellOrderPrice);
                }
            }

            if (this.gridsTypes === 1 && this.customGridsSell.length !== 0) {
                if (this.longShort === 0 || this.longShort === 2) {
                    const obj = {};
                    // obj.slSize = this.checkSizesSell[this.checkSizesSell.length - 1] + this.customGridsBuySlSize;
                    obj.slSize = this.checkSizesSell[this.checkSizesSell.length - 1] * 1;
                    obj.slPrice = this.lastSellOrderPrice;
                    await this.setBuyStopLoss(obj.slSize, obj.slPrice);
                }
            }

            for (let objBuy of this.gridBuy) {
                await this.setBuyLimit(objBuy.Size, objBuy.Price);
            }

            for (let objSell of this.gridSell) {
                await this.setSellLimit(objSell.Size, objSell.Price);
            }
        }

        // STOPLOSS + BUY + NO PAUSE
        if (this.hedgeOrStoploss === 1 && this.activePosition.result.direction === 'buy' && this.flagActRobotPause === 0) {
            if (this.longShort === 0) {
                const obj = {};
                if (this.gridsTypes === 0) {
                    obj.slSize = this.checkSizes[this.checkSizes.length - 1] + this.gridOrderSize;
                    this.lastSellOrderPrice = this.gridSell[this.gridSell.length - 1].Price + this.stoplossStep;
                    obj.slPrice = this.lastSellOrderPrice;
                    await this.setBuyStopLoss(obj.slSize, obj.slPrice);
                }

                if (this.gridsTypes === 1 && this.customGridsSell.length !== 0) {
                    // obj.slSize = this.checkSizesSell[this.checkSizesSell.length - 1] + this.customGridsBuySlSize;
                    obj.slSize = this.checkSizesSell[this.checkSizesSell.length - 1] * 1;
                    this.lastSellOrderPrice = this.gridSell[this.gridSell.length - 1].Price  + parseFloat(this.customGridsBuySlPrice);
                    obj.slPrice = this.lastSellOrderPrice;
                    await this.setBuyStopLoss(obj.slSize, obj.slPrice);
                }

            }

            for (let objSell of this.gridSell) {
                await this.setSellLimit(objSell.Size, objSell.Price);
            }
        }

        // STOPLOSS + SELL + NO PAUSE
        if (this.hedgeOrStoploss === 1 && this.activePosition.result.direction === 'sell' && this.flagActRobotPause === 0) {
            if (this.longShort === 0) {
                const obj = {};
                if (this.gridsTypes === 0) {
                    obj.slSize = this.checkSizes[this.checkSizes.length - 1] + this.gridOrderSize;
                    this.lastBuyOrderPrice = this.gridBuy[this.gridBuy.length - 1].Price - this.stoplossStep;
                    if (this.lastBuyOrderPrice <= 0) this.lastBuyOrderPrice = 0.5;
                    obj.slPrice = this.lastBuyOrderPrice;
                    console.log('setSellStopLoss ###### 2',obj.slSize, obj.slPrice);
                    await this.setSellStopLoss(obj.slSize, obj.slPrice);
                }

                if (this.gridsTypes === 1 && this.customGridsBuy.length !== 0) {
                    // obj.slSize = this.checkSizesBuy[this.checkSizesBuy.length - 1] + this.customGridsSellSlSize;
                    obj.slSize = this.checkSizesBuy[this.checkSizesBuy.length - 1] * 1;
                    this.lastBuyOrderPrice = this.gridBuy[this.gridBuy.length - 1].Price - this.customGridsSellSlPrice;
                    if (this.lastBuyOrderPrice <= 0) this.lastBuyOrderPrice = 0.5;
                    obj.slPrice = this.lastBuyOrderPrice;
                    await this.setSellStopLoss(obj.slSize, obj.slPrice);
                }
            }

            for (let objBuy of this.gridBuy) {
                await this.setBuyLimit(objBuy.Size, objBuy.Price);
            }
        }
        // ================== END ===================
        // SET STOPLOSSS ORDERS

        return false;
    };

    async cancelTakeGrid(type) {
        await this.getOpenOrders();
        if (this.accessToken !== '') {
            // Direction buy
            if (type === 'sell') {
                while (this.openOrdersSells.length !== 0) {
                    await Promise.all(
                        this.openOrdersSells.map(async (objSell) => {
                            return await this.cancelOrderOne(objSell.Id);
                        })
                    );
                    await this.getOpenOrders();
                }

                if (this.flagActRobotPause === 0) {
                    if (this.openOrdersStoplossBuyIds.length !== 0) {
                        while (this.openOrdersStoplossBuyIds.length !== 0) {
                            await this.cancelOrderOne(this.openOrdersStoplossBuyIds[0].Id);
                            await this.getOpenOrders();
                        }
                    }
                }
            }

            // Direction sell
            if (type === 'buy') {
                while (this.openOrdersBuys.length !== 0) {
                    await Promise.all(
                        this.openOrdersBuys.map(async (objBuy) => {
                            return await this.cancelOrderOne(objBuy.Id);
                        })
                    );
                    await this.getOpenOrders();
                }

                if (this.flagActRobotPause === 0) {
                    if (this.openOrdersStoplossSellIds.length !== 0) {
                        while (this.openOrdersStoplossSellIds.length !== 0) {
                            await this.cancelOrderOne(this.openOrdersStoplossSellIds[0].Id);
                            await this.getOpenOrders();
                        }
                    }
                }
            }
        }
    }

    async setSettings() {
        const mysql = new Mysql();
        const resultGR = await mysql.getGrids();
        // console.log(result);
        let gridStep = 0;
        let gridOrders = 0;
        let gridOrderSize = 0;
        let gridOrderSizePer = 0;
        let gridFirstOrder = 0;
        let hedgeOffset = 0;
        let stepMultiplier = 1;
        let sizeMultiplier = 1;
        let hedgeOrStoploss = 0;
        let stoplossStep = 50;
        let customTake = 0;
        let longShort = 0;
        let stepMultiplierSign = 0;
        let sizeMultiplierSign = 0;

        if (typeof resultGR !== "undefined") {
            Object.keys(resultGR).forEach(function (key) {
                stoplossStep = this[key]['stp_step'];

                const prefix = this[key]['ti_name'].toLowerCase();
                gridStep = this[key][`${prefix}_grid_step`];
                gridOrders = this[key][`${prefix}_grid_orders`];
                gridOrderSize = this[key][`${prefix}_grid_order_size`];
                gridOrderSizePer = this[key][`${prefix}_grid_order_size_per`];
                gridFirstOrder = this[key][`${prefix}_first_order`];
                hedgeOffset = this[key][`${prefix}_hedge_offset`];
                stepMultiplier = this[key][`${prefix}_step_multiplier`];
                sizeMultiplier = this[key][`${prefix}_size_multiplier`];
                customTake = this[key][`${prefix}_custom_take`];
                longShort = this[key][`${prefix}_long_short`];
                stepMultiplierSign = this[key][`${prefix}_step_multiplier_sign`];
                sizeMultiplierSign = this[key][`${prefix}_size_multiplier_sign`];
            }, resultGR);
        }

        if (gridStep !== 0 && gridOrders !== 0 && gridOrderSize !== 0 && hedgeOffset !== 0) {
            this.gridStep = gridStep;
            this.gridOrders = gridOrders;
            this.gridOrderSize = gridOrderSize;
            this.gridOrderSizePer = gridOrderSizePer;
            this.gridFirstOrder = gridFirstOrder;
            this.hedgeOffset = hedgeOffset;
            this.stepMultiplier = stepMultiplier;
            this.sizeMultiplier = sizeMultiplier;

            this.stoplossStep = stoplossStep;

            this.customTake = customTake;

            this.longShort = longShort;

            this.stepMultiplierSign = stepMultiplierSign;
            this.sizeMultiplierSign = sizeMultiplierSign;
        } else {
            console.error("Something wrong. Grids are 0.");
        }

        const resultHS = await mysql.getHedgeStoploss();
        if (typeof resultHS !== "undefined") {
            Object.keys(resultHS).forEach(function (key) {
                hedgeOrStoploss = this[key]['usr_hedge_or_stoploss'];
            }, resultHS);
        }
        this.hedgeOrStoploss = hedgeOrStoploss;


        await mysql.doneMysql();
    }

    static async parseJsonAsyncFunc(jsonString) {
        return await JSON.parse(jsonString);
    }

    static isEmpty(obj) {
        for (let key in obj) {
            return false;
        }
        return true;
    }

    static isOdd(num) {
        return num % 2;
    }
}

module.exports = Api;