require('dotenv').config();

const fetch = require('node-fetch');

const Redis = require('ioredis');
const redis = new Redis({
    port: process.env.DREDIS_PORT,
    host: process.env.DREDIS_HOST,
    password: process.env.DREDIS_PASSWORD,
    db: process.env.DREDIS_DB,
});

let accessToken = '';
let expiresIn = 0;
let counterForExpiresIn = 0;

let buyPrices = [];
let sellPrices = [];

const globalOrderSize = 5;
const hedgeOffset = 50;
const time = 60;

let headers = {};

const auth = async () => {
    const url = 'https://test.deribit.com/api/v2/';
    const method = 'public/auth';
    const params = `?username=${process.env.DUSERNAME}&password=${process.env.DPASSWORD}&grant_type=password&scope=${process.env.DSESSION}`;
    const res = await fetch(`${url}${method}${params}`);
    const body = await res.json();
    accessToken = body.result.access_token;
    expiresIn = body.result.expires_in;

    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Bearer ${accessToken}`
    };

    clearTimeout(authTimer);

    return false;
};

const getTicker = async () => {
    const url = 'https://test.deribit.com/api/v2/';
    const method = 'public/ticker';
    const params = '?instrument_name=BTC-PERPETUAL';
    const res = await fetch(`${url}${method}${params}`, {
        headers: headers,
    });
    const body = await res.json();

    return body.result.last_price;
};

const cancelOrders = async () => {
    const url = 'https://test.deribit.com/api/v2/';
    const method = 'private/cancel_all_by_currency';
    const params = '?currency=BTC';
    const res = await fetch(`${url}${method}${params}`, {
        headers: headers,
    });
    await res.json();

    return false;
};

const setBuyLimit = async (orderSize, orderPrice) => {
    const url = 'https://test.deribit.com/api/v2/';
    const method = 'private/buy';
    const params = `?amount=${orderSize}&instrument_name=BTC-PERPETUAL&type=limit&price=${orderPrice}&post_only=true`;
    const res = await fetch(`${url}${method}${params}`, {
        headers: headers,
    });
    await res.json();

    return false;
};

const setSellLimit = async (orderSize, orderPrice) => {
    const url = 'https://test.deribit.com/api/v2/';
    const method = 'private/sell';
    const params = `?amount=${orderSize}&instrument_name=BTC-PERPETUAL&type=limit&price=${orderPrice}&post_only=true`;
    const res = await fetch(`${url}${method}${params}`, {
        headers: headers,
    });
    await res.json();

    return false;
};

const hedgeBuy = async (size) => {
    const url = 'https://test.deribit.com/api/v2/';
    const method = 'private/buy';
    const params = `?amount=${size}&instrument_name=BTC-28JUN19&type=market`;
    const res = await fetch(`${url}${method}${params}`, {
        headers: headers,
    });

    return await res.json();
}

const hedgeSell = async(size) => {
    const url = 'https://test.deribit.com/api/v2/';
    const method = 'private/sell';
    const params = `?amount=${size}&instrument_name=BTC-28JUN19&type=market`;
    const res = await fetch(`${url}${method}${params}`, {
        headers: headers,
    });

    return await res.json();
}

const getPosition = async () => {
    const url = 'https://test.deribit.com/api/v2/';
    const method = 'private/get_position';
    const params = '?instrument_name=BTC-PERPETUAL';
    const res = await fetch(`${url}${method}${params}`, {
        headers: headers,
    });

    return await res.json();
};

const checkHedgePosition = async () => {
    const url = 'https://test.deribit.com/api/v2/';
    const method = 'private/get_position';
    const params = '?instrument_name=BTC-28JUN19';
    const res = await fetch(`${url}${method}${params}`, {
        headers: headers,
    });

    const body = await res.json();

    return body.result.size;
};

const checkAuth = async () => {
    counterForExpiresIn += time;

    if (counterForExpiresIn >= 800) {
        counterForExpiresIn = 0;
        await auth();
        console.log('Update TOKEN:', new Date());
    }

    return false;
};

const initBuySellPricesArrs = async (lastPrice, buyAdd = true, sellAdd = true) => {
    buyPrices = [];
    sellPrices = [];

    for (let i = 1; i <= 15; i++) {
        let xN = i * 10;
        if (buyAdd) buyPrices.push(lastPrice - xN);
        if (sellAdd) sellPrices.push(lastPrice + xN);
    }

    const result = await redis.pipeline()
        .set('var:buys', JSON.stringify(buyPrices))
        .set('var:sells', JSON.stringify(sellPrices));

    result.catch((error) => {
        console.error(error);
    });

    return false;
};

const activePositionResultSize = async () => {
    await cancelOrders();

    for (let price of buyPrices) {
        await setBuyLimit(globalOrderSize * 10, price);
    }

    for (let price of sellPrices) {
        await setSellLimit(globalOrderSize * 10, price);
    }

    return false;
};

const start = async () => {
    await checkAuth();

    const activePosition = await getPosition();

    const lastPrice = await getTicker();

    await initBuySellPricesArrs(lastPrice);

    if (activePosition.result.size === 0) {
        await activePositionResultSize();
    }

    if (activePosition.result.direction === 'buy'){
        // console.log('direction:', activePosition.result.direction);
        const firstOrderSize = Math.abs(activePosition.result.size);
        const firstOrderPrice = activePosition.result.average_price + 10;
        let counterBuy = globalOrderSize * (150 - firstOrderSize);
        const slPriceBuy = firstOrderPrice - hedgeOffset;
        await cancelOrders();

        let hedgePosition = 0;
        if (lastPrice < slPriceBuy){
            hedgePosition = await checkHedgePosition();
            // console.log('hedgePosition buy, <:', hedgePosition, typeof hedgePosition);
            if (hedgePosition === 0){
                console.log('buy: hedgeSell');
                await hedgeSell(globalOrderSize * 150);
            }
        } else {
            hedgePosition = await checkHedgePosition();
            // console.log('hedgePosition buy, >=:', hedgePosition);
            if (hedgePosition !== 0){
                console.log('buy: hedgeBuy');
                await hedgeBuy(hedgePosition);
            }
        }

        await setSellLimit(firstOrderSize, firstOrderPrice);

        for (let price of buyPrices) {
            if (counterBuy !== 0){
                await setBuyLimit(globalOrderSize * 10, price);
                counterBuy -= (globalOrderSize * 10);
            }
        }
    }

    if (activePosition.result.direction === 'sell'){
        console.log('direction:', activePosition.result.direction);
        const firstOrderSize = Math.abs(activePosition.result.size);
        const firstOrderPrice = activePosition.result.average_price - 10;
        let counterSell = globalOrderSize * (150 - firstOrderSize);
        const slPriceSell = firstOrderPrice + hedgeOffset;
        await cancelOrders();

        let hedgePosition = 0;
        if (lastPrice > slPriceSell) {
            console.log('hedgePosition sell, >:', hedgePosition);
            hedgePosition = await checkHedgePosition();
            if (hedgePosition === 0) {
                console.log('sell: hedgeBuy');
                await hedgeBuy(globalOrderSize * 150);
            }
        } else {
            hedgePosition = await checkHedgePosition();
            console.log('hedgePosition sell, <:', hedgePosition);
            if (hedgePosition !== 0) {
                console.log('sell: hedgeSell');
                await hedgeSell(hedgePosition);
            }
        }

        await setBuyLimit(firstOrderSize, firstOrderPrice);

        for (let price of sellPrices) {
            if (counterSell !== 0){
                await setSellLimit(globalOrderSize * 10, price);
                counterSell -= (globalOrderSize * 10);
            }
        }
    }
};

const authTimer = setTimeout(auth, 1000);
setInterval(start, time * 1000);