const fetch = require('node-fetch');
const WebSocket = require('ws');
const ws = new WebSocket('wss://test.deribit.com/ws/api/v2');

let access_key = '';
let usDiff = 0;

function getCountdown(secondsLeft){
    secondsLeft = secondsLeft / 1000;

    const days = pad( parseInt(secondsLeft / 86400) );
    secondsLeft = secondsLeft % 86400;

    const hours = pad( parseInt(secondsLeft / 3600) );
    secondsLeft = secondsLeft % 3600;

    const minutes = pad( parseInt(secondsLeft / 60) );
    const seconds = pad( parseInt( secondsLeft % 60 ) );

    const obj = {};
    obj.Day = days;
    obj.Hours = hours;
    obj.Minutes = minutes;
    obj.Seconds = seconds;
    return obj;
}

function pad(n) {
    return (n < 10 ? '0' : '') + n;
}

const auth =
    {
        "jsonrpc" : "2.0",
        "method" : "public/auth",
        "params" : {
            "grant_type": "password",
            "username": "test@djamour.com",
            "password": "Var562245",
            "scope": "session:name12"
        }
    };

const getPosition =
    {
        "jsonrpc" : "2.0",
        "method" : "private/get_position",
        "params" : {
            "instrument_name" : "BTC-PERPETUAL"
        }
    };

const ticker =
    {
        "jsonrpc" : "2.0",
        "method" : "public/ticker",
        "params" : {
            "instrument_name" : "BTC-PERPETUAL"
        }
    };

ws.addEventListener('open', function (e) {
    ws.send(JSON.stringify(auth));
});

const buyPrices = [];
const sellPrices = [];

const aFunc = async () => {
    const activePosition = await ws.send(JSON.stringify(getPosition));
    const result1 = await ws.send(JSON.stringify(ticker));
    const lastPrice = result1.result.last_price;
    for (let i = 1; i <= 15; i++) {
        const x10 = i * 10;
        buyPrices.push(lastPrice - x10);
        sellPrices.push(lastPrice + x10);
    }

    if(activePosition.size === '0'){
        console.log('size', activePosition.size);
    }


}

setInterval(aFunc, 5000);


ws.addEventListener('message', function (e) {
    const json = JSON.parse(e.data);
    console.log(json);
    console.log(' -------------------- ');
    /*if (typeof json.result.access_token !== 'undefined') {
        console.log(json);
        access_key = json.result.access_token;
        console.log(' -------------------- ');
        console.log(access_key);
        console.log(' -------------------- ');
        usDiff = json.usDiff;
        const resultTime = getCountdown(usDiff);
        console.log(resultTime);
        ws.send(JSON.stringify(getPosition));
    }*/
    // ws.send(JSON.stringify(getPosition));
});

