/*
const Redis = require('ioredis');
const redis = new Redis({
    port: process.env.DREDIS_PORT,
    host: process.env.DREDIS_HOST,
    password: process.env.DREDIS_PASSWORD,
    db: process.env.DREDIS_DB,
});
*/

require('dotenv').config();
const Api = require('./api');
const Mysql = require('./mysql');
const Redis = require('ioredis');
const Restapi = require('./restapi');

let counterForExpiresIn = 0;
let users = [];
let apis = [];

function deepEqual (obj1, obj2) {
    return JSON.stringify(obj1) === JSON.stringify(obj2);
}

async function getUsersFromMysql() {
    let res = "undefined";
    let resultRedisLocal = "undefined";
    let redisLocal = {};

    const restapi = new Restapi();
    res = await restapi.getUserSettings();
    resultRedisLocal = await restapi.getRedisSettings(2);

    if (res === null && resultRedisLocal === null) {
        try {
            const mysql = new Mysql();
            res = await mysql.getUserSettings();
            resultRedisLocal = await mysql.getRedisSettings(2);
            await mysql.doneMysql();
        } catch (err) {
            console.log('Mysql, getUsersFromMysql(): error query api', err.message);
        }
    }

    if (typeof resultRedisLocal !== "undefined" && resultRedisLocal !== null) {
        redisLocal = new Redis({
            port: resultRedisLocal[0]['rds_port'],
            host: resultRedisLocal[0]['rds_host'],
            password: resultRedisLocal[0]['rds_password'],
            db: resultRedisLocal[0]['rds_db'],
            family: 4
        });
    }

    const usersInit = [];
    // console.log( ' ========================= ');
    // console.log(typeof res, res);

    if (typeof res !== "undefined" && res !== null) {
        Object.keys(res).forEach(function (key) {
            let domain = 'www.deribit.com';
            const prefix = this[key]['ti_name'].toLowerCase();

            if (this[key][`${prefix}_domain`] === 0 || this[key][`${prefix}_domain`] === null) {
                domain = 'www.deribit.com';
            } else {
                domain = 'test.deribit.com';
            }

            const obj = {};
            obj.id = this[key]['user_id'];
            if (this[key][`${prefix}_domain`] === 0 || this[key][`${prefix}_domain`] === null) {
                obj.clientId = this[key]['usr_client_id'];
                obj.clientSecret = this[key]['usr_client_secret'];
            } else {
                obj.clientId = this[key]['usr_client_id_test'];
                obj.clientSecret = this[key]['usr_client_secret_test'];
            }
            obj.redis = redisLocal;
            obj.expiresIn = counterForExpiresIn;
            obj.session = `session:${this[key]['usr_username']}${this[key]['ti_name'].toUpperCase()}prod`;
            obj.domain = domain;
            obj.active = this[key]['usr_freeze_button'];
            obj.gridStep = this[key][`${prefix}_grid_step`];
            obj.gridOrders = this[key][`${prefix}_grid_orders`];
            obj.gridOrderSize = this[key][`${prefix}_grid_order_size`];
            obj.hedgeOffset = this[key][`${prefix}_hedge_offset`];
            obj.stepMultiplier = this[key][`${prefix}_step_multiplier`];
            obj.sizeMultiplier = this[key][`${prefix}_size_multiplier`];
            obj.robotStatus = this[key]['usr_robot_status'];
            obj.hedgeOrStoploss = this[key]['usr_hedge_or_stoploss'];
            obj.stoplossStep = this[key]['stp_step'];
            obj.usrExpired = this[key]['usr_expired'];
            obj.customTake = this[key][`${prefix}_custom_take`];
            obj.longShort = this[key][`${prefix}_long_short`];
            obj.stepMultiplierSign = this[key][`${prefix}_step_multiplier_sign`];
            obj.sizeMultiplierSign = this[key][`${prefix}_size_multiplier_sign`];
            obj.gridOrderSizePer = this[key][`${prefix}_grid_order_size_per`];
            obj.gridFirstOrder = this[key][`${prefix}_first_order`];
            obj.sideLevelLow = this[key][`${prefix}_level_side_low`];
            obj.sideLevelHigh = this[key][`${prefix}_level_side_high`];
            obj.gridsTypes = this[key][`${prefix}_grids_types`];
            obj.customGridsBuy = this[key][`${prefix}_custom_grids_buy`];
            obj.customGridsSell = this[key][`${prefix}_custom_grids_sell`];
            obj.customGridsBuySlPrice = this[key][`${prefix}_custom_grids_buy_sl_price`];
            obj.customGridsBuySlSize = this[key][`${prefix}_custom_grids_buy_sl_size`];
            obj.customGridsSellSlPrice = this[key][`${prefix}_custom_grids_sell_sl_price`];
            obj.customGridsSellSlSize = this[key][`${prefix}_custom_grids_sell_sl_size`];
            obj.socksIp = this[key]['cnt_sock'];
            obj.socksPort = this[key]['cnt_sock_port'];
            usersInit.push(obj);
        }, res);
    }

    return usersInit;
}

async function setUsersFreezeButton(status1, status2) {
    // const mysql = new Mysql();
    // await mysql.setUserFreezeButton(status1, status2);
    // await mysql.doneMysql();

    const restapi = new Restapi();
    const result = await restapi.setUserFreezeButton(status1, status2);
    console.log('setUserFreezeButton():', result);
}

async function setApis() {
    // const mysqlRedis = new Mysql();
    // const resultRedis = await mysqlRedis.getRedisSettings(1);
    // await mysqlRedis.doneMysql();

    let resultRedis = "undefined";

    const restapi = new Restapi();
    resultRedis = await restapi.getRedisSettings(1);

    if (resultRedis === null) {
        try {
            const mysql = new Mysql();
            resultRedis = await mysql.getRedisSettings(1);
            await mysql.doneMysql();
        } catch (err) {
            console.log('Mysql, resultRedis(): error mysql api', err.message);
        }
    }

    if (typeof resultRedis !== "undefined" && resultRedis !== null) {
        const redissub = new Redis({
            port: resultRedis[0]['rds_port'],
            host: resultRedis[0]['rds_host'],
            password: resultRedis[0]['rds_password'],
            db: resultRedis[0]['rds_db'],
        });

        const redispub = new Redis({
            port: resultRedis[0]['rds_port'],
            host: resultRedis[0]['rds_host'],
            password: resultRedis[0]['rds_password'],
            db: resultRedis[0]['rds_db'],
        });

        for (let objUser of users) {
            if (objUser.robotStatus) {
                const apiAuth = new Api(
                    objUser.clientId,
                    objUser.clientSecret,
                    objUser.redis,
                    objUser.expiresIn,
                    objUser.session,
                    objUser.domain,
                    objUser.gridStep,
                    objUser.gridOrders,
                    objUser.gridOrderSize,
                    objUser.hedgeOffset,
                    objUser.id,
                    objUser.stepMultiplier,
                    objUser.sizeMultiplier,
                    objUser.hedgeOrStoploss,
                    redissub,
                    redispub,
                    objUser.stoplossStep,
                    objUser.usrExpired,
                    objUser.customTake,
                    objUser.longShort,
                    objUser.stepMultiplierSign,
                    objUser.sizeMultiplierSign,
                    objUser.gridOrderSizePer,
                    objUser.gridFirstOrder,
                    objUser.sideLevelLow,
                    objUser.sideLevelHigh,
                    objUser.gridsTypes,
                    objUser.customGridsBuy,
                    objUser.customGridsSell,
                    objUser.customGridsBuySlPrice,
                    objUser.customGridsBuySlSize,
                    objUser.customGridsSellSlPrice,
                    objUser.customGridsSellSlSize,
                    objUser.socksIp,
                    objUser.socksPort,
                );
                let obj = {};
                obj.Userid = objUser.id;
                obj.Obj = apiAuth;
                apis.push(obj);
            }
        }
    }
}

let globalStopFlagNative = 0;

const authTimer = setTimeout(async () => {
    console.log('Step #1: script starts.');
    let resultUsers = await getUsersFromMysql();
    if (process.env.D_USER_ID === '6') {
        console.log('getUsersFromMysql :::::', resultUsers)
    }
    // if (resultUsers.length !== 0 && resultUsers[0].active && !resultUsers[0].robotStatus) {
    if (resultUsers.length !== 0 && resultUsers[0].robotStatus === 1) {
        users = resultUsers.slice();

        await setApis();
        for (let api of apis) {
            if (apis.length !== 0) {
                console.log('Step #2: init datas:');
                // console.log(api);
                const resultsGetFromRedis = await api.Obj.getDataForReboot();
                console.log('resultsGetFromRedis #1', resultsGetFromRedis);
                if (
                    resultsGetFromRedis[0][1] !== null &&
                    resultsGetFromRedis[1][1] !== null &&
                    resultsGetFromRedis[1][1] !== '' &&
                    (resultsGetFromRedis[1][1] === 'buy' || resultsGetFromRedis[1][1] === 'sell')
                ) {
                    const foo1 = JSON.parse(resultsGetFromRedis[0][1]);
                    const foo2 = resultsGetFromRedis[1][1];
                    const { activePositionOldSize } = foo1;
                    const { directionOld } = foo2;
                    api.Obj.activePositionOldSize = parseFloat(activePositionOldSize);
                    api.Obj.directionOld = directionOld;
                }
                await api.Obj.removeDataForReboot();
                const resultsGetFromRedis2 = await api.Obj.getDataForReboot();
                console.log('resultsGetFromRedis #2', resultsGetFromRedis2);
                await api.Obj.auth();
                await setUsersFreezeButton(0, 1);

                api.Obj.redisSub.subscribe(`p_${process.env.D_USER_ID}`, `upse_${process.env.D_USER_ID}`, `upreboot_${process.env.D_USER_ID}`, () => {
                    api.Obj.redisSub.on('message', async (channel, message) => {
                        if (message !== '') {
                            // const json = await Api.parseJsonAsyncFunc(message);
                            console.log(channel, message, typeof message);

                            // if (json.Status === '1') {
                            if (channel === `p_${process.env.D_USER_ID}`) {
                                api.Obj.flagActRobotPause = parseFloat(message);
                                await api.Obj.checkAuth('redis-check');
                                if (api.Obj.accessToken !== '') {
                                    await api.Obj.getPosition();
                                    api.Obj.flagPauseWhenDerebitDisRedis = 0;
                                    if (api.Obj.activePosition.result.direction === 'zero') {
                                        api.Obj.globalStopFlag = 1;
                                        globalStopFlagNative = 1;
                                        console.log('Direction is zero #1.');
                                        console.log('Robot was stoped #1.');
                                        // api.Obj.redisSub.subscribe('start', async () => {
                                        api.Obj.redisPub.publish('start', `s_${process.env.D_USER_ID}`);
                                        const restapi = new Restapi();
                                        const res0 = await restapi.emptySocks();
                                        console.log('emptySocks() #1:', res0);
                                        const mysql = new Mysql();
                                        const resConIp = await mysql.getIpContainer();
                                        await mysql.setUserFreezeButton(0, 0);
                                        await mysql.setPauseRobot(0);
                                        // await Promise.all([mysql.setUserFreezeButton(0, 0), mysql.setPauseRobot(0)]);
                                        await mysql.doneMysql();
                                        await api.Obj.cancelOrders();
                                        // process.exit(0);
                                        await api.Obj.stopContainer(resConIp);
                                    }

                                    /*
                                    if (api.Obj.activePosition.result.direction === 'buy') {
                                        let objSell = {};
                                        objSell.Price = api.Obj.gridSell[0].Price;
                                        objSell.Size = api.Obj.gridSell[0].Size;
                                        api.Obj.gridSell = [];
                                        api.Obj.gridSell.push(objSell);

                                        await api.Obj.cancelOrders('limit');
                                        await api.Obj.setOrders();
                                    }

                                    if (api.Obj.activePosition.result.direction === 'sell') {
                                        let objBuy = {};
                                        objBuy.Price = api.Obj.gridBuy[0].Price;
                                        objBuy.Size = api.Obj.gridBuy[0].Size;
                                        api.Obj.gridBuy = [];
                                        api.Obj.gridBuy.push(objBuy);

                                        await api.Obj.cancelOrders('limit');
                                        await api.Obj.setOrders();
                                    }
                                    */
                                } else {
                                    api.Obj.flagPauseWhenDerebitDisRedis = 1;
                                    console.log('flagPauseWhenDerebitDisRedis:', api.Obj.flagPauseWhenDerebitDisRedis);
                                }
                            }

                            if (channel === `upse_${process.env.D_USER_ID}`) {
                                api.Obj.getGridSettings = 1;
                                console.log(channel, message, typeof message, api.Obj.getGridSettings);
                                await api.Obj.setRedis(message);
                            }

                            if (channel === `upreboot_${process.env.D_USER_ID}`) {
                                console.log(channel, message, typeof message);
                                await api.Obj.keepDataForReboot();
                                const restApiUpReboot = new Restapi();
                                const resConIp = await restApiUpReboot.getIpContainer();
                                await api.Obj.restartContainer(resConIp);
                            }

                            if (channel === `upsecg_${process.env.D_USER_ID}`) {
                                api.Obj.getCustomGridsSettings = 1;
                                console.log(channel, message, typeof message, api.Obj.getGridSettings);
                            }
                        }
                    });
                });
            }
        }
    }

    const greeting = function delay() {
        setTimeout(async () => {
            // START
            // console.time('Total time');
            // console.log('Start ...');
            if (apis.length !== 0) {
                for (let api of apis) {
                    // await api.delRedis();
                    // console.time('Script works');
                    if (api.Obj.globalStopFlag === 0 && globalStopFlagNative === 0) {
                        if (api.Obj.accessToken !== '' && typeof api.Obj.accessToken !== "undefined") {
                            if (api.Obj.flagPauseWhenDerebitDisRedis === 1) {
                                console.log('flagPauseWhenDerebitDisRedis = 1');
                                api.Obj.flagPauseWhenDerebitDisRedis = 0;
                                let obj = {};
                                obj.Id = process.env.D_USER_ID;
                                obj.Status = '1';
                                obj.Action = 're-auth-pause';
                                api.Obj.redisPub.publish('re-auth-pause', JSON.stringify(obj));
                            } else {
                                await api.Obj.checkAuth('just-check');

                                // await api.Obj.getTicker();
                                if (api.Obj.sideLevelLow !== 0 && api.Obj.sideLevelHigh !== 0) {
                                    await api.Obj.getTicker();
                                    if (api.Obj.lastPrice >= api.Obj.sideLevelLow && api.Obj.lastPrice < api.Obj.sideLevelHigh) {
                                        api.Obj.longShort = 0;
                                    }

                                    if (api.Obj.lastPrice < api.Obj.sideLevelLow) {
                                        api.Obj.longShort = 1;
                                    }

                                    if (api.Obj.lastPrice >= api.Obj.sideLevelHigh) {
                                        api.Obj.longShort = 2;
                                    }
                                }
                                await api.Obj.getPosition();
                                // await api.Obj.getHedgePosition();

                                /*
                                await Promise.all([
                                    api.Obj.getPosition(),
                                    api.Obj.getHedgePosition()
                                ]);
                                */

                                // console.log('activePosition.result:', typeof api.Obj.activePosition.result);
                                if (typeof api.Obj.activePosition.result !== "undefined" && api.Obj.accessToken !== '') {

                                    if (
                                        api.Obj.activePosition.result.direction !== 'zero' &&
                                        api.Obj.directionOld !== api.Obj.activePosition.result.direction
                                    ) {
                                        api.Obj.activePositionOldSize = 0;
                                        api.Obj.directionOld = '';
                                    }

                                    if (api.Obj.activePosition.result.direction === 'zero' && api.Obj.accessToken !== '' && api.Obj.errorSellBuy === 0) {
                                        api.Obj.activePositionOldSize = 0;
                                        api.Obj.directionOld = '';

                                        if (api.Obj.firstZeroMoving === 0) {
                                            api.Obj.firstZeroMoving = 1;
                                        }

                                        if (api.Obj.hedgeOrStoploss === 1) {
                                            await api.Obj.getOpenOrders();
                                            const quantityStoplossOpenOrders = api.Obj.amountBuyStopDirection + api.Obj.amountSellStopDirection;

                                            if (api.Obj.accessToken !== '') {
                                                if (api.Obj.longShort === 0) {
                                                    if (Api.isOdd(quantityStoplossOpenOrders) === 1 && (api.Obj.amountBuyDirection === 0 || api.Obj.amountSellDirection === 0)) {
                                                        api.Obj.globalStopFlag = 1;
                                                        console.log('Stoploss was active!');
                                                        // await api.Obj.cancelOrders();
                                                        await api.Obj.cancelOrdersByInstrument('PERPETUAL');
                                                        const restapi = new Restapi();
                                                        const res0 = await restapi.emptySocks();
                                                        console.log('emptySocks() #2:', res0);
                                                        const resConIp = await restapi.getIpContainer();
                                                        console.log('SL getIpContainer():', resConIp);
                                                        const res1 = await restapi.setStoplossTimerActive(1, api.Obj.gridsTypes);
                                                        console.log('SL setStoplossTimerActive():', res1);
                                                        const res2 = await restapi.setTypeOfEmail(2);
                                                        console.log('SL setTypeOfEmail():', res2);
                                                        const res3 = await restapi.setUserFreezeButton(0, 0);
                                                        console.log('SL setUserFreezeButton():', res3);

                                                        // const mysql = new Mysql();
                                                        // const resConIp = await mysql.getIpContainer();
                                                        // await mysql.setStoplossTimerActive(1);
                                                        // await mysql.setTypeOfEmail(2);
                                                        // await mysql.setUserFreezeButton(0, 0);

                                                        // await Promise.all([mysql.setStoplossTimerActive(1), mysql.setTypeOfEmail(2), mysql.setUserFreezeButton(0, 0)]);
                                                        // await mysql.doneMysql();
                                                        await api.Obj.stopContainer(resConIp);
                                                        // process.exit(0);
                                                    } else {
                                                        await api.Obj.setGrids();
                                                    }
                                                }

                                                if (api.Obj.longShort !== 0) {
                                                    if (
                                                        quantityStoplossOpenOrders === 0 &&
                                                        (
                                                            // LONG, buy
                                                            (api.Obj.amountBuyDirection === 0 && api.Obj.amountSellDirection !== 0) ||
                                                            // SHORT, sell
                                                            (api.Obj.amountSellDirection === 0 && api.Obj.amountBuyDirection !== 0)
                                                        )
                                                    ) {
                                                        api.Obj.globalStopFlag = 1;
                                                        console.log('Stoploss was active!');
                                                        // await api.Obj.cancelOrders();
                                                        await api.Obj.cancelOrdersByInstrument('PERPETUAL');
                                                        const restapi = new Restapi();
                                                        const res0 = await restapi.emptySocks();
                                                        console.log('emptySocks() #3:', res0);
                                                        const mysql = new Mysql();
                                                        const resConIp = await mysql.getIpContainer();
                                                        // await Promise.all([
                                                        //     await mysql.setStoplossTimerActive(1),
                                                        //     await mysql.setTypeOfEmail(2),
                                                        //     await mysql.setUserFreezeButton(0, 0)
                                                        // ]);
                                                        await mysql.setStoplossTimerActive(1, api.Obj.gridsTypes);
                                                        await mysql.setTypeOfEmail(2);
                                                        await mysql.setUserFreezeButton(0, 0);
                                                        await mysql.doneMysql();
                                                        await api.Obj.stopContainer(resConIp);
                                                    } else {
                                                        await api.Obj.setGrids();
                                                    }
                                                }
                                            } else {
                                                console.log('Something wrong with auth token #1. ZERO SECTION');
                                                await api.Obj.checkAuth('again');
                                            }
                                        }
                                    }

                                    if (api.Obj.activePosition.result.direction === 'buy' && api.Obj.accessToken !== '' && api.Obj.errorSellBuy === 0) {
                                        api.Obj.firstZeroMoving = 0;

                                        if (api.Obj.hedgeOrStoploss === 1) {
                                            if (
                                                Math.abs(api.Obj.activePositionOldSize) < Math.abs(api.Obj.activePosition.result.size) ||
                                                api.Obj.directionOld !== api.Obj.activePosition.result.direction
                                            ) {
                                                await api.Obj.cancelTakeGrid('sell');
                                                if (api.Obj.accessToken !== '') {
                                                    await api.Obj.getPosition();
                                                    await api.Obj.setGrids();
                                                    await api.Obj.setOrders();
                                                }
                                                if (api.Obj.accessToken !== '') {
                                                    api.Obj.activePositionOldSize = Math.abs(api.Obj.activePosition.result.size);
                                                    api.Obj.directionOld = api.Obj.activePosition.result.direction;
                                                    console.log('Robot is waiting stoploss SELL.');
                                                } else {
                                                    console.log('Something wrong with auth token. BUY SECTION');
                                                    await api.Obj.checkAuth('again');
                                                }
                                            }
                                        }
                                    }

                                    if (api.Obj.activePosition.result.direction === 'sell' && api.Obj.accessToken !== '' && api.Obj.errorSellBuy === 0) {
                                        api.Obj.firstZeroMoving = 0;

                                        if (api.Obj.hedgeOrStoploss === 1) {
                                            if (Math.abs(api.Obj.activePositionOldSize) < Math.abs(api.Obj.activePosition.result.size) ||
                                                api.Obj.directionOld !== api.Obj.activePosition.result.direction) {
                                                await api.Obj.cancelTakeGrid('buy');
                                                if (api.Obj.accessToken !== '') {
                                                    await api.Obj.getPosition();
                                                    await api.Obj.setGrids();
                                                    await api.Obj.setOrders();
                                                }
                                                if (api.Obj.accessToken !== '') {
                                                    api.Obj.activePositionOldSize = Math.abs(api.Obj.activePosition.result.size);
                                                    api.Obj.directionOld = api.Obj.activePosition.result.direction;
                                                    console.log('Robot is waiting stoploss BUY.');
                                                } else {
                                                    console.log('Something wrong with auth token. SELL SECTION');
                                                    await api.Obj.checkAuth('again');
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            await api.Obj.checkAuth('re-auth');
                        }
                    } else {
                        console.log('Robot almost ready stop/restart ...', api.Obj.globalStopFlag, globalStopFlagNative);
                        clearTimeout(greeting);
                    }
                    // console.timeEnd('Script works');
                    greeting();
                }
            }
            // console.timeEnd('Total time');
        }, 1000);
    }

    greeting();
}, 500);