require('dotenv').config();
const fetch = require('node-fetch');

class Restapi {
    constructor(...params) {
        this.headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        };
    }

    async getUserSettings() {
        try {
            const body = {
                tiName: process.env.D_INSTRUMENT_NAME.toLowerCase(),
                usId: process.env.D_USER_ID,
            };

            const url = `https://deribot.info/api/index.php`;
            const params = '?query=gus';
            const res = await fetch(`${url}${params}`, {
                method: 'post',
                body: JSON.stringify(body),
                headers: this.headers,
            });

            return await res.json();
        } catch (err) {
            console.error('getUserSettings() error #1:', err.message);
            return '';
        }
    };

    async getRedisSettings(id) {
        try {
            const body = {
                rId: id,
            };

            const url = `https://deribot.info/api/`;
            const params = '?query=grs';
            const res = await fetch(`${url}${params}`, {
                method: 'post',
                body: JSON.stringify(body),
                headers: this.headers,
            });

            return await res.json();
        } catch (err) {
            console.error('getRedisSettings() error #1:', err.message);
            return '';
        }
    }

    async setUserFreezeButton(status1, status2) {
        try {
            const body = {
                status1: status1,
                status2: status2,
                usId: process.env.D_USER_ID,
            };

            const url = `https://deribot.info/api/`;
            const params = '?query=sufb';
            const res = await fetch(`${url}${params}`, {
                method: 'post',
                body: JSON.stringify(body),
                headers: this.headers,
            });

            return await res.json();
        } catch (err) {
            console.error('setUserFreezeButton() error #1:', err.message);
            return '';
        }
    }

    async setPauseRobot(state) {
        try {
            const body = {
                state: state,
                usId: process.env.D_USER_ID,
            };

            const url = `https://deribot.info/api/`;
            const params = '?query=spr';
            const res = await fetch(`${url}${params}`, {
                method: 'post',
                body: JSON.stringify(body),
                headers: this.headers,
            });

            return await res.json();
        } catch (err) {
            console.error('setPauseRobot() error #1:', err.message);
            return '';
        }
    }

    async getIpContainer() {
        try {
            const body = {
                usId: process.env.D_USER_ID,
            };

            const url = `https://deribot.info/api/`;
            const params = '?query=gic';
            const res = await fetch(`${url}${params}`, {
                method: 'post',
                body: JSON.stringify(body),
                headers: this.headers,
            });

            return await res.json();
        } catch (err) {
            console.error('getIpContainer() error #1:', err.message);
            return '';
        }
    }

    async setStoplossTimerActive(state, gridsTypes = 0) {
        try {
            const body = {
                state: state,
                gridsTypes: gridsTypes,
                usId: process.env.D_USER_ID,
            };

            const url = `https://deribot.info/api/`;
            const params = '?query=ssta';
            const res = await fetch(`${url}${params}`, {
                method: 'post',
                body: JSON.stringify(body),
                headers: this.headers,
            });

            return await res.json();
        } catch (err) {
            console.error('setStoplossTimerActive() error #1:', err.message);
            return '';
        }
    }

    async setTypeOfEmail(type) {
        try {
            const body = {
                type: type,
                usId: process.env.D_USER_ID,
            };

            const url = `https://deribot.info/api/`;
            const params = '?query=stof';
            const res = await fetch(`${url}${params}`, {
                method: 'post',
                body: JSON.stringify(body),
                headers: this.headers,
            });

            return await res.json();
        } catch (err) {
            console.error('setStoplossTimerActive() error #1:', err.message);
            return '';
        }
    }

    async hedgeInfo(foo) {
        try {
            const body = {
                step: foo.step,
                instrument: foo.instrument,
                directionOfInstrument: foo.directionOfInstrument,
                type: foo.type,
                balance: foo.balance,
                balanceNew: foo.balanceNew,
                subtraction: foo.subtraction,
                size: foo.size,
                price: foo.price,
                markPerpetual: foo.markPerpetual,
                indexPerpetual: foo.indexPerpetual,
                lastPerpetual: foo.lastPerpetual,
                markHedge: foo.markHedge,
            };

            const url = `https://deribot.info/api/`;
            const params = '?query=hinf';
            const res = await fetch(`${url}${params}`, {
                method: 'post',
                body: JSON.stringify(body),
                headers: this.headers,
            });

            return await res.json();
        } catch (err) {
            console.error('hedgeInfo() error #1:', foo.step, err.message);
            return '';
        }
    }

    async emptySocks() {
        try {
            const body = {
                usId: process.env.D_USER_ID,
            };

            const url = `https://deribot.info/api/`;
            const params = '?query=uhscks';
            const res = await fetch(`${url}${params}`, {
                method: 'post',
                body: JSON.stringify(body),
                headers: this.headers,
            });

            return await res.json();
        } catch (err) {
            console.error('emptySocks() error #1:', err.message);
            return '';
        }
    }
}

module.exports = Restapi;