FROM node:carbon

# Install software 
RUN apt-get install -y git
RUN npm install forever -g
VOLUME /der

RUN git config --global user.email "alari777@gmail.com"
RUN git config --global user.name "alari777"

RUN git clone -b master --single-branch https://bitbucket.org/alari777/der.git /der/

# Create app directory
WORKDIR /der

EXPOSE 80
CMD git pull origin master && npm install -y && npm start